/** AddModToDegreePage.java for adding modules to a degree
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import user.Degree;
import user.Level;

public class AddModToDegreePage extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField txtModCode;
	private JTextField txtLevel;
	private JCheckBox chkOptional;
	private JButton btnSubmit;
	
	private Degree degree;
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 * @param degree 
	 */
	public AddModToDegreePage(Degree degree) {
		this.degree = degree;
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel_1.add(new JLabel("Module Code"));
		txtModCode = new JTextField();
		txtModCode.setColumns(10);
		panel_1.add(txtModCode);
		panel.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.add(new JLabel("Period"));
		txtLevel = new JTextField();
		txtLevel.setColumns(10);
		panel_2.add(txtLevel);
		panel.add(panel_2);
		
		JPanel panel_3 = new JPanel();
		chkOptional = new JCheckBox("Optional");
		panel_3.add(chkOptional);
		panel.add(panel_3);
		
		btnSubmit = new JButton("Link module to degree");
		btnSubmit.addActionListener(this);
		panel.add(btnSubmit);
		contentPane.add(panel);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			new Level(degree, txtLevel.getText().charAt(0), new user.Module(degree.getDeparment().getCode() ,Integer.parseInt(txtModCode.getText())), chkOptional.isSelected());
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dispose();
	}

}
