/** CreateUserPage.java for creating a user
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import user.Administrator;
import user.User;
import user.Student;

public class CreateUserPage extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField txtUsername, txtForename, txtSurname, txtEmail, txtTitle, txtDepartment, txtDegree, txtTutor, txtUndergrad;
	private JPasswordField txtPassword;
	private JComboBox gradList;
	private JPanel panel_1;
	private JPanel panel_5;
	private JPanel panel_6;
	private JPanel panel_7;
	private JPanel panel_8;
	private JPanel panel_9;
	private JPanel panel_10;
	private JPanel panel_11;
	private JPanel panel_12;
	private JPanel panel_13;
	
	private User user;

	/**
	 * Create the frame.
	 * @param user
	 */
	public CreateUserPage(User user) {
		this.user = user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		JLabel lblUserType = new JLabel("User Type");
		panel_2.add(lblUserType);
		
		JComboBox userList;
		if (user.getClass().equals(Administrator.class)) {
			String[] userTypes = {"Student", "Teacher", "Registrar", "Administrator"};
			userList = new JComboBox(userTypes);
		} else {
			String[] userTypes = {"Student"};
			userList = new JComboBox(userTypes);
		}
		
		userList.addActionListener(this);
		panel_2.add(userList);

		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);

		JLabel lblUsername = new JLabel("Username");
		panel_2.add(lblUsername);

		txtUsername = new JTextField();
		panel_2.add(txtUsername);
		txtUsername.setColumns(10);

		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);

		JLabel lblPassword = new JLabel("Password");
		panel_4.add(lblPassword);

		txtPassword = new JPasswordField();
		txtPassword.setColumns(10);
		panel_4.add(txtPassword);

		JPanel panel_5 = new JPanel();
		panel_2.add(panel_5);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().getClass().equals(JComboBox.class)) {
			JComboBox cb = (JComboBox)arg0.getSource();
			String userType = (String)cb.getSelectedItem();
			if (userType.equals("Student")) {
				panel_1.removeAll();
				contentPane.revalidate();
				panel_5 = new JPanel();
				panel_1.add(panel_5);

				JLabel lblForename = new JLabel("Forename");
				panel_5.add(lblForename);

				txtForename = new JTextField();
				txtForename.setColumns(10);
				panel_5.add(txtForename);

				panel_6 = new JPanel();
				panel_1.add(panel_6);

				JLabel lblSurname = new JLabel("Surname");
				panel_6.add(lblSurname);

				txtSurname = new JTextField();
				txtSurname.setColumns(10);
				panel_6.add(txtSurname);

				panel_8 = new JPanel();
				panel_1.add(panel_8);

				JLabel lblTitle = new JLabel("Title");
				panel_8.add(lblTitle);

				txtTitle = new JTextField();
				txtTitle.setColumns(10);
				panel_8.add(txtTitle);

				panel_9 = new JPanel();
				panel_1.add(panel_9);

				JLabel lblDepartment = new JLabel("Department code");
				panel_9.add(lblDepartment);

				txtDepartment = new JTextField();
				txtDepartment.setColumns(10);
				panel_9.add(txtDepartment);

				panel_10 = new JPanel();
				panel_1.add(panel_10);

				JLabel lblDegree = new JLabel("Degree code");
				panel_10.add(lblDegree);

				txtDegree = new JTextField();
				txtDegree.setColumns(10);
				panel_10.add(txtDegree);

				panel_11 = new JPanel();
				panel_1.add(panel_11);
				
				JLabel lblGrad = new JLabel("Grad");
				panel_11.add(lblGrad);
				
				if (user.getClass().equals(Administrator.class)) {
					String[] gradTypes = {"U", "P", "MSc"};
					gradList = new JComboBox(gradTypes);
				} else {
					String[] gradTypes = {"Student"};
					gradList = new JComboBox(gradTypes);
				}
				panel_11.add(gradList);
				

				panel_12 = new JPanel();
				panel_1.add(panel_12);

				JLabel lblTutor = new JLabel("Tutor");
				panel_12.add(lblTutor);

				txtTutor = new JTextField();
				txtTutor.setColumns(10);
				panel_12.add(txtTutor);
				
				panel_13 = new JPanel();
				panel_1.add(panel_13);
				
				JButton btnAdd = new JButton("Add student");
				btnAdd.addActionListener(this);
				panel_13.add(btnAdd);
				
				this.setSize(250, 480);
				panel_1.validate();
								
			} else if (userType.equals("Teacher")) {
				panel_1.removeAll();
				contentPane.revalidate();
				panel_5 = new JPanel();
				panel_1.add(panel_5);

				JLabel lblForename = new JLabel("Forename");
				panel_5.add(lblForename);

				txtForename = new JTextField();
				txtForename.setColumns(10);
				panel_5.add(txtForename);

				panel_6 = new JPanel();
				panel_1.add(panel_6);

				JLabel lblSurname = new JLabel("Surname");
				panel_6.add(lblSurname);

				txtSurname = new JTextField();
				txtSurname.setColumns(10);
				panel_6.add(txtSurname);

				panel_7 = new JPanel();
				panel_1.add(panel_7);

				JLabel lblEmail = new JLabel("Email");
				panel_7.add(lblEmail);

				txtEmail = new JTextField();
				txtEmail.setColumns(10);
				panel_7.add(txtEmail);

				panel_8 = new JPanel();
				panel_1.add(panel_8);

				JLabel lblTitle = new JLabel("Title");
				panel_8.add(lblTitle);

				txtTitle = new JTextField();
				txtTitle.setColumns(10);
				panel_8.add(txtTitle);

				panel_9 = new JPanel();
				panel_1.add(panel_9);

				JLabel lblDepartment = new JLabel("Department code");
				panel_9.add(lblDepartment);

				txtDepartment = new JTextField();
				txtDepartment.setColumns(10);
				panel_9.add(txtDepartment);

				panel_10 = new JPanel();
				panel_1.add(panel_10);

				panel_11 = new JPanel();
				panel_1.add(panel_11);

				panel_12 = new JPanel();
				panel_1.add(panel_12);

				panel_13 = new JPanel();
				panel_1.add(panel_13);
				
				JButton btnAdd = new JButton("Add teacher");
				panel_13.add(btnAdd);
				
				this.setSize(250, 480);
				panel_1.validate();
					
			} else if (userType.equals("Registrar")) {
				panel_1.removeAll();
				contentPane.revalidate();
				panel_5 = new JPanel();
				panel_1.add(panel_5);

				JLabel lblForename = new JLabel("Forename");
				panel_5.add(lblForename);

				txtForename = new JTextField();
				txtForename.setColumns(10);
				panel_5.add(txtForename);

				panel_6 = new JPanel();
				panel_1.add(panel_6);

				JLabel lblSurname = new JLabel("Surname");
				panel_6.add(lblSurname);

				txtSurname = new JTextField();
				txtSurname.setColumns(10);
				panel_6.add(txtSurname);

				panel_7 = new JPanel();
				panel_1.add(panel_7);

				JLabel lblEmail = new JLabel("Email");
				panel_7.add(lblEmail);

				txtEmail = new JTextField();
				txtEmail.setColumns(10);
				panel_7.add(txtEmail);

				panel_8 = new JPanel();
				panel_1.add(panel_8);

				JLabel lblTitle = new JLabel("Title");
				panel_8.add(lblTitle);

				txtTitle = new JTextField();
				txtTitle.setColumns(10);
				panel_8.add(txtTitle);

				panel_9 = new JPanel();
				panel_1.add(panel_9);

				panel_10 = new JPanel();
				panel_1.add(panel_10);

				panel_11 = new JPanel();
				panel_1.add(panel_11);

				panel_12 = new JPanel();
				panel_1.add(panel_12);

				panel_13 = new JPanel();
				panel_1.add(panel_13);
				
				JButton btnAdd = new JButton("Add registrar");
				panel_13.add(btnAdd);
				
				this.setSize(250, 480);
				panel_1.validate();
				System.out.println();
					
			} else if (userType.equals("Administrator")) {
				panel_1.removeAll();
				contentPane.revalidate();
				panel_5 = new JPanel();
				panel_1.add(panel_5);

				JLabel lblForename = new JLabel("Forename");
				panel_5.add(lblForename);

				txtForename = new JTextField();
				txtForename.setColumns(10);
				panel_5.add(txtForename);

				panel_6 = new JPanel();
				panel_1.add(panel_6);

				JLabel lblSurname = new JLabel("Surname");
				panel_6.add(lblSurname);

				txtSurname = new JTextField();
				txtSurname.setColumns(10);
				panel_6.add(txtSurname);

				panel_7 = new JPanel();
				panel_1.add(panel_7);

				JLabel lblEmail = new JLabel("Email");
				panel_7.add(lblEmail);

				txtEmail = new JTextField();
				txtEmail.setColumns(10);
				panel_7.add(txtEmail);

				panel_8 = new JPanel();
				panel_1.add(panel_8);

				JLabel lblTitle = new JLabel("Title");
				panel_8.add(lblTitle);

				txtTitle = new JTextField();
				txtTitle.setColumns(10);
				panel_8.add(txtTitle);

				panel_9 = new JPanel();
				panel_1.add(panel_9);

				panel_10 = new JPanel();
				panel_1.add(panel_10);

				panel_11 = new JPanel();
				panel_1.add(panel_11);

				panel_12 = new JPanel();
				panel_1.add(panel_12);

				panel_13 = new JPanel();
				panel_1.add(panel_13);
				
				JButton btnAdd = new JButton("Add administrator");
				panel_13.add(btnAdd);
				
				this.setSize(250, 480);
				panel_1.validate();
			} else {
				this.setSize(250,200);
				panel_1.remove(panel_5);
				panel_1.remove(panel_6);
				panel_1.remove(panel_7);
				panel_1.remove(panel_8);
				panel_1.remove(panel_9);
				panel_1.remove(panel_10);
				panel_1.remove(panel_11);
				panel_1.remove(panel_12);
				JButton btnAdd = new JButton(userType);
				btnAdd.addActionListener(this);
				panel_13.removeAll();
				panel_13.add(btnAdd);
				panel_1.validate();
			}
		} else if (arg0.getSource().getClass().equals(JButton.class)) {
			JButton button = (JButton) arg0.getSource();
			if (button.getText().equalsIgnoreCase("Add teacher")) {
				try {
					new User(txtUsername.getText(), txtPassword.getText(), "Teacher");
				} catch (NoSuchAlgorithmException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (button.getText().equalsIgnoreCase("Add administrator")) {
				try {
					new User(txtUsername.getText(), txtPassword.getText(), "Admin");
				} catch (NoSuchAlgorithmException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			} else if (button.getText().equalsIgnoreCase("Add student")) {
				try {
					try {
						new Student(txtUsername.getText(), txtPassword.getText(), txtForename.getText(), txtSurname.getText(), txtTitle.getText(), txtTutor.getText(),  txtDepartment.getText(), gradList.getSelectedItem().toString(), Integer.parseInt(txtDegree.getText()));
					} catch (NumberFormatException | NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				//addUser(username, password, student, details);
			} else if (button.getText().equalsIgnoreCase("Add registrar")) {
				try {
					new User(txtUsername.getText(), txtPassword.getText(), "Registrar");
				} catch (NoSuchAlgorithmException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			this.dispose();
		}
	}
}
