package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import user.Level;
import user.Module;
import user.ModulePick;
import user.ModuleRequest;
import user.Period;
import user.Student;
import user.User;

public class EditStudentModulePage extends JFrame implements ActionListener{
	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JFrame welcome;
	private ArrayList<JCheckBox> checkList;
	private JTextField txtStudentReg;
	
	private ArrayList<Module> moduleList;
	private User user;
	private Period currPeriod;
	
	private int creditsAvailable;
	
	public EditStudentModulePage(JFrame frame, User user) {
		this.user = user;
		this.welcome = frame;
		
		checkList = new ArrayList<JCheckBox>();
		moduleList = new ArrayList<Module>();
				
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 851, 139);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		if (user.getClass().equals(Student.class)) {
			Student student = (Student) user;
			panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			try {
				ArrayList<Period> periodList = Period.getAll(student);
				if (periodList.size() == 0) {
					Level level = new Level(student.getDegree(), 'A');
					
					List<ModulePick> coreModules = level.getCores();
					List<ModulePick> optionalModules = level.getOptionals();
					
					currPeriod = new Period(student.getRegistration(), new java.sql.Date(0), new java.sql.Date(0), 'A', false);
					int totalCredits = 0;
					for (int i=0; i<coreModules.size(); i++) {
						new ModuleRequest(student.getRegistration(), currPeriod.getPeriodKey(), coreModules.get(i));
						totalCredits += coreModules.get(i).getCredits();
						currPeriod.addModule(coreModules.get(i));
					}
					
					if (optionalModules.size() > 0 && totalCredits < 120) {
						for (int i=0; i<optionalModules.size(); i++) {
							JCheckBox chkModule = new JCheckBox(optionalModules.get(i).getTitle());
							checkList.add(chkModule);
							moduleList.add(optionalModules.get(i));
							panel.add(chkModule);
						}
						JButton btnAddModule = new JButton("Request modules");
						creditsAvailable = 120 - totalCredits;
						btnAddModule.addActionListener(this);
						panel.add(btnAddModule);
					} else {
						panel.add(new JLabel("There are no optional modules for this period, you have been registered."));
					}
					
				} else if (periodList.get(periodList.size() - 1).passed() == true && periodList.get(periodList.size() - 1).getLevelKey() != 'D') {
					panel = new JPanel();
					panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
					
					Level level = new Level(student.getDegree(), (char)((int)periodList.get(periodList.size() - 1).getLevelKey() + 1));
					
					List<ModulePick> coreModules = level.getCores();
					List<ModulePick> optionalModules = level.getOptionals();
					
					currPeriod = new Period(student.getRegistration(), (java.sql.Date) new Date(Date.parse("")), null, (char)((int)periodList.get(periodList.size() - 1).getLevelKey() + 1), false);
					int totalCredits = 0;
					for (int i=0; i<coreModules.size(); i++) {
						currPeriod.addModule(coreModules.get(i));
						totalCredits += coreModules.get(i).getCredits();
					}
					
					if (optionalModules.size() > 0 && totalCredits < 120) {
						for (int i=0; i<optionalModules.size(); i++) {
							JCheckBox chkModule = new JCheckBox(optionalModules.get(i).getTitle());
							panel.add(chkModule);
						}
						JButton btnAddModule = new JButton("Request modules");
						creditsAvailable = 120 - totalCredits;
						btnAddModule.addActionListener(this);
						panel.add(btnAddModule);
					} else {
						panel.add(new JLabel("There are no optional modules for this period, you have been registered."));
					}
					
				} 
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Period moduleList1 = Period.getLast(student);
				displayTable(Period.getLast(student).getModules());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			contentPane.add(panel);
		} else {
			
			JPanel panel_1 = new JPanel();
			panel_1.add(new JLabel("Student registration no"));
			txtStudentReg = new JTextField();
			txtStudentReg.setColumns(10);
			panel_1.add(txtStudentReg);
			panel.add(panel_1);
			JButton btnSubmit = new JButton("Submit");
			btnSubmit.addActionListener(this);
			panel.add(btnSubmit);
		}
		contentPane.add(panel);
	}

	private void displayTable(ArrayList<Module> dataList) {
		String[] columnNames = {"Title", "Department code", "Semester", "Module code", "Credits", "Taught"};
		try {
			panel.removeAll();
		} catch (Exception e) {
			//is fine
		}
		Object[][] data;
		data = new Object[dataList.size()][6];
		for (int i=0; i< dataList.size(); i++) {
			data[i][0] = dataList.get(i).getTitle();
			data[i][1] = dataList.get(i).getDepartmentCode();
			data[i][2] = dataList.get(i).getSemester();
			data[i][3] = dataList.get(i).getModuleCode();
			data[i][4] = dataList.get(i).getCredits();
			data[i][5] = dataList.get(i).isTaught();
		}
		JTable table = new JTable(data, columnNames);
		panel.add(table);
		contentPane.revalidate();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) arg0.getSource()).getText() == "Request modules") {
			Student student = (Student) user;
			int totalCredits = 0;
			for (int i=0; i<checkList.size(); i++) {
				if (checkList.get(i).isSelected()) {
					totalCredits += moduleList.get(i).getCredits();
				}
			}
			if (totalCredits > creditsAvailable) {
				JOptionPane.showMessageDialog(null, "Not enough credits available for your module choices", "Not enough credits", 
						JOptionPane.ERROR_MESSAGE);
			} else {
				for (int i=0; i<moduleList.size();i++) {
					if (checkList.get(i).isSelected()) {
						try {
							new ModuleRequest(student.getRegistration(), currPeriod.getPeriodKey(), moduleList.get(i));
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					JOptionPane.showMessageDialog(null, "You are now registered for this period", "Period registered!", 
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		} else {
			try {
				ArrayList<Period> periodList = Period.getAll(new Student(Integer.parseInt(txtStudentReg.getText())));
				displayTable(periodList.get(periodList.size() - 1).getModules());
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
