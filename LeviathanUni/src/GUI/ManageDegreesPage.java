package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import GUI.ManageDepartmentsPage.AddEditDel;
import user.Administrator;
import user.Degree;
import user.Department;
import user.User;

public class ManageDegreesPage extends JFrame implements ActionListener, MouseListener{

	private JPanel contentPane;
	private JPanel panel_1;
	private JPanel panel_2;
	private JFrame welcome;
	private JCheckBox chkIndustry;
	private JButton btnAdd;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnModule;
	private JButton btnLink;
	private JButton btnSubmit;
	private JTextField txtId;
	private JTextField txtDepCode;
	private JTextField txtDegreeCode;
	private JTextField txtDegreeName;
	private JTextField txtCredits;
	private JTextField txtPartnered;
	private String[] data;
	private JComboBox depCodeList;
	private JComboBox gradList;

	
	private String origDepCode;
	private int origDegCode;
	
	private Administrator user;
	private AddEditDel edit;
	/**
	 * Create the frame.
	 */
	public ManageDegreesPage(JFrame welcome, User user) {
		this.welcome = welcome;
		this.user = (Administrator) user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(this);
		btnBack.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnBack);
		
		panel_1 = new JPanel();
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		panel_1.add(panel_2);
		displayTable();
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(new FlowLayout());
		btnAdd = new JButton("Add Degree");
		btnAdd.addActionListener(this);
		btnEdit = new JButton("Edit Degree");
		btnEdit.addActionListener(this);
		btnDelete = new JButton("Delete Degree");
		btnDelete.addActionListener(this);
		btnModule = new JButton("Add module to degree");
		btnModule.setVisible(false);
		btnModule.addActionListener(this);
		panel_1.add(panel_3);
		panel_3.add(btnDelete);
		panel_3.add(btnEdit);
		panel_3.add(btnAdd);
		panel_3.add(btnModule);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		panel_1.add(panel_4);	
		
		JPanel panel_6 = new JPanel();
		
		JLabel lblDepCode = new JLabel("Department Code");
		panel_6.add(lblDepCode);
		txtDepCode = new JTextField();
		txtDepCode.setColumns(10);
		
		panel_6.add(txtDepCode);
		panel_4.add(panel_6);
		
		JPanel panel_7 = new JPanel();
		panel_7.add(new JLabel("Degree Code"));
		txtDegreeCode = new JTextField();
		txtDegreeCode.setColumns(10);
		panel_7.add(txtDegreeCode);
		panel_4.add(panel_7);
		
		JPanel panel_8 = new JPanel();
		panel_8.add(new JLabel("Degree Name"));
		txtDegreeName = new JTextField();
		txtDegreeName.setColumns(10);
		panel_8.add(txtDegreeName);
		panel_4.add(panel_8);
//		
		JPanel panel_9 = new JPanel();
		JLabel lblGrad = new JLabel("Grad");
		panel_9.add(lblGrad);

		if (user.getClass().equals(Administrator.class)) {
			String[] gradTypes = {"U", "P", "MSc"};
			gradList = new JComboBox(gradTypes);
		} else {
			String[] gradTypes = {"Student"};
			gradList = new JComboBox(gradTypes);
		}
		panel_9.add(gradList);
		panel_4.add(panel_9);
		
		
		JPanel panel_10 = new JPanel();
		chkIndustry = new JCheckBox("Year in Industry");
		panel_10.add(chkIndustry);
		panel_4.add(panel_10);
		
		JPanel panel_11 = new JPanel();
		panel_11.add(new JLabel("Partnered Departments"));
		txtPartnered = new JTextField();
		txtPartnered.setColumns(10);
		panel_11.add(txtPartnered);
		panel_4.add(panel_11);
		
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(this);
		btnSubmit.setEnabled(false);
		panel_4.add(btnSubmit);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Back") {
			welcome.setVisible(true);
			this.dispose();
		} else if (((JButton) (arg0.getSource())).equals(btnAdd) || ((JButton) (arg0.getSource())).equals(btnEdit) || ((JButton) (arg0.getSource())).equals(btnDelete))  {
			if (((JButton) (arg0.getSource())).equals(btnAdd)) {
				edit = AddEditDel.ADD;
				btnAdd.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnEdit)) {
				edit = AddEditDel.EDIT;
				btnEdit.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnDelete)) {
				edit = AddEditDel.DELETE;
				btnDelete.setVisible(false);
			}
			btnSubmit.setEnabled(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.ADD) {
			//user.addCourse(txtDegreeName.getText(), Integer.parseInt(txtCredits.getText()), txtDegreeCode.getText(), true); //Need to fix to add module name
			try {
				Department dept = new Department(txtDepCode.getText());
				String partners = txtPartnered.getText();
				partners = partners.trim();
				partners = partners.toUpperCase();
				partners = partners.replaceAll(" ", "");
				String[] depts= partners.split(",");
				
				ArrayList<Department> dlist = new ArrayList<Department>();
				try {
					for(String s : depts) {
						dlist.add(new Department(s));
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
				new Degree(txtDegreeName.getText(), dept, Integer.parseInt(txtDegreeCode.getText()), chkIndustry.isSelected(), ((String) gradList.getSelectedItem()).charAt(0), dlist);
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			displayTable();
			btnAdd.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.EDIT) {
			try {
				Degree degree = new Degree(origDepCode, origDegCode);
				degree.update(txtDegreeName.getName(), txtDepCode.getText(), Integer.parseInt(txtDegreeCode.getText()), chkIndustry.isSelected(), txtCredits.getText().charAt(0));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			displayTable();
			btnEdit.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.DELETE) {
			try {
				Degree degree = new Degree(origDepCode, origDegCode);
				degree.delete();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			displayTable();
			btnDelete.setVisible(true);
		} else if (arg0.getSource().equals(btnModule)) {
			try {
				Degree degree = new Degree(origDepCode, origDegCode);
				AddModToDegreePage add = new AddModToDegreePage(degree);
				add.setVisible(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		contentPane.revalidate();
	}

	private void displayTable() {
		String[] columnNames = {"Department code", "Degree Code", "Degree Name", "Undergrad", "Year in industry"};
		Object[][] data;
		panel_2.removeAll();
		try {
			ArrayList<Degree> dataList = Degree.getAll();
			data = new Object[dataList.size()][5];
			for (int i=0; i< dataList.size(); i++) {
				data[i][0] = dataList.get(i).getDeparment().getCode();
				data[i][1] = dataList.get(i).getDegreeCode();
				data[i][2] = dataList.get(i).getName();
				data[i][3] = dataList.get(i).getUnderPost();
				data[i][4] = dataList.get(i).getIndustry();
			}
			JTable table = new JTable(data, columnNames);
			table.addMouseListener(this);
			panel_2.add(table);
		} catch (SQLException e) {
			welcome.setVisible(true);
			this.dispose();
			e.printStackTrace();
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent mouseEvent) {
        JTable table =(JTable) mouseEvent.getSource();
        Point point = mouseEvent.getPoint();
        int row = table.rowAtPoint(point);
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
        		origDepCode = (String)table.getModel().getValueAt(table.getSelectedRow(), 0);
        		origDegCode = (int)table.getModel().getValueAt(table.getSelectedRow(), 1);
        		txtDepCode.setText((String) table.getModel().getValueAt(table.getSelectedRow(), 0));
        		txtDegreeCode.setText("" + table.getModel().getValueAt(table.getSelectedRow(), 1));
        		txtDegreeName.setText((String)table.getModel().getValueAt(table.getSelectedRow(), 2));
        		chkIndustry.setSelected((boolean) table.getModel().getValueAt(table.getSelectedRow(), 4));
        		
        		btnModule.setVisible(true);
        }
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

}
