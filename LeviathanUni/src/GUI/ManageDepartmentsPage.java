/** ManageDepartmentsPage.java for managing departments
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import user.Administrator;
import user.Department;
import user.User;

public class ManageDepartmentsPage extends JFrame implements ActionListener, MouseListener{

	private JPanel contentPane;
	private JFrame welcome;
	private JPanel panel_2;
	private Administrator user;
	private JTextField txtDepCode;
	private JTextField txtDepName;
	private JPanel panel_1;
	private JButton btnAdd, btnEdit, btnDelete, btnSubmit;
	
	private String deptEditCode;
	
	public enum AddEditDel { ADD, EDIT, DELETE };
	private AddEditDel edit;
	
	/**
	 * Create the frame.
	 */
	public ManageDepartmentsPage(JFrame welcome, User user) {
		this.welcome = welcome;
		this.user = (Administrator) user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(this);
		btnBack.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnBack);
		
		panel_1 = new JPanel();
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		panel_1.add(panel_2);
		displayTable();
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(new FlowLayout());
		btnAdd = new JButton("Add department");
		btnAdd.addActionListener(this);
		btnEdit = new JButton("Edit department");
		btnEdit.addActionListener(this);
		btnDelete = new JButton("Delete deparment");
		btnDelete.addActionListener(this);
		panel_1.add(panel_3);
		panel_3.add(btnDelete);
		panel_3.add(btnEdit);
		panel_3.add(btnAdd);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		panel_1.add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5);
		panel_5.add(new JLabel("Department Code"));
		txtDepCode = new JTextField();
		txtDepCode.setColumns(10);
		panel_5.add(txtDepCode);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.add(new JLabel("Deparment Name"));
		txtDepName = new JTextField();
		txtDepName.setColumns(10);
		panel_6.add(txtDepName);
		
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(this);
		btnSubmit.setEnabled(false);
		panel_4.add(btnSubmit);
	}
	
	/**
	 * For displaying the table that contains all degree information extracted from the database
	 */
	private void displayTable() {
		String[] columnNames = {"Department code", "Department name"};
		String[][] data;
		panel_2.removeAll();
		try {
			ArrayList<Department> dataList = Department.getAll();
			data = new String[dataList.size()][2];
			for (int i=0; i< dataList.size(); i++) {
				data[i][0] = dataList.get(i).getCode();
				data[i][1] = dataList.get(i).getName();
			}
			JTable table = new JTable(data, columnNames);
			table.addMouseListener(this);
			panel_2.add(table);
		} catch (SQLException e) {
			welcome.setVisible(true);
			this.dispose();
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Back") {
			welcome.setVisible(true);
			this.dispose();
		} else if (((JButton) (arg0.getSource())).equals(btnAdd) || ((JButton) (arg0.getSource())).equals(btnEdit) || ((JButton) (arg0.getSource())).equals(btnDelete))  {		
			if (((JButton) (arg0.getSource())).equals(btnAdd)) {
				edit = AddEditDel.ADD;
				btnAdd.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnEdit)) {
				edit = AddEditDel.EDIT;
				btnEdit.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnDelete)) {
				edit = AddEditDel.DELETE;
				btnDelete.setVisible(false);
			}
			btnSubmit.setEnabled(true);
			
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.ADD) {
			try {
				new Department(txtDepName.getText(), txtDepCode.getText());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnAdd.setVisible(true);
			displayTable();
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.EDIT) {
			try {
				Department dept = new Department(deptEditCode);
				dept.update(txtDepCode.getText(), txtDepName.getText());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnEdit.setVisible(true);
			displayTable();
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.DELETE) {
			try {
				Department dept = new Department(deptEditCode);
				dept.delete();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnDelete.setVisible(true);
			displayTable();
		}
		contentPane.revalidate();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent mouseEvent) {
        JTable table =(JTable) mouseEvent.getSource();
        Point point = mouseEvent.getPoint();
        int row = table.rowAtPoint(point);
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
        		deptEditCode = (String)table.getModel().getValueAt(table.getSelectedRow(), 0);
        		txtDepCode.setText(deptEditCode);
        		txtDepName.setText((String)table.getModel().getValueAt(table.getSelectedRow(), 1));
        }
        contentPane.revalidate();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	

}
