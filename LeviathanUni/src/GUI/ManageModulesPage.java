/** ManageModulesPage.java for managing modules
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import GUI.ManageDepartmentsPage.AddEditDel;
import user.Administrator;
import user.Module;
import user.User;

public class ManageModulesPage extends JFrame implements ActionListener, MouseListener{

	private JPanel contentPane;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_4;
	private JFrame welcome;
	private JButton btnAdd;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnSubmit;
	private JCheckBox chkTaught;
	private JTextField txtTitle;
	private JTextField txtDepCode;
	private JTextField txtSemester;
	private JTextField txtModCode;
	private JTextField txtCredits;
	
	private int origModCode;
	
	private Administrator user;
	private AddEditDel edit;
	/**
	 * Create the frame.
	 */
	public ManageModulesPage(JFrame welcome, User user) {
		this.welcome = welcome;
		this.user = (Administrator) user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(this);
		btnBack.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnBack);
		
		panel_1 = new JPanel();
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		panel_1.add(panel_2);
		displayTable();
				
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(new FlowLayout());
		btnAdd = new JButton("Add module");
		btnAdd.addActionListener(this);
		btnEdit = new JButton("Edit module");
		btnEdit.addActionListener(this);
		btnDelete = new JButton("Delete module");
		btnDelete.addActionListener(this);
		panel_1.add(panel_3);
		panel_3.add(btnDelete);
		panel_3.add(btnEdit);
		panel_3.add(btnAdd);
		
		panel_4 = new JPanel();
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		panel_1.add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5);
		panel_5.add(new JLabel("Title"));
		txtTitle = new JTextField();
		txtTitle.setColumns(10);
		panel_5.add(txtTitle);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.add(new JLabel("Department Code"));
		txtDepCode = new JTextField();
		txtDepCode.setColumns(10);
		panel_6.add(txtDepCode);
		
		JPanel panel_7 = new JPanel();
		panel_4.add(panel_7);
		panel_7.add(new JLabel("Semester"));
		txtSemester = new JTextField();
		txtSemester.setColumns(10);
		panel_7.add(txtSemester);
		
		JPanel panel_8 = new JPanel();
		panel_4.add(panel_8);
		panel_8.add(new JLabel("Module Code"));
		txtModCode = new JTextField();
		txtModCode.setColumns(10);
		panel_8.add(txtModCode);
		
		JPanel panel_9 = new JPanel();
		panel_4.add(panel_9);
		panel_9.add(new JLabel("Credits"));
		txtCredits = new JTextField();
		txtCredits.setColumns(10);
		panel_9.add(txtCredits);
		
		JPanel panel_10 = new JPanel();
		panel_4.add(panel_10);
		chkTaught = new JCheckBox("Taught");
		panel_10.add(chkTaught);
		
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(this);
		btnSubmit.setEnabled(false);
		panel_4.add(btnSubmit);
	}

	/**
	 * For displaying the table that contains all module information extracted from the database
	 */
	private void displayTable() {
		String[] columnNames = {"Title", "Department code", "Semester", "Module code", "Credits", "Taught"};
		try {
			panel_2.removeAll();
		} catch (Exception e) {
			//is fine
		}
		Object[][] data;
		try {
			ArrayList<Module> dataList = Module.getAll();
			data = new Object[dataList.size()][6];
			for (int i=0; i< dataList.size(); i++) {
				data[i][0] = dataList.get(i).getTitle();
				data[i][1] = dataList.get(i).getDepartmentCode();
				data[i][2] = dataList.get(i).getSemester();
				data[i][3] = dataList.get(i).getModuleCode();
				data[i][4] = dataList.get(i).getCredits();
				data[i][5] = dataList.get(i).isTaught();
			}
			JTable table = new JTable(data, columnNames);
			table.addMouseListener(this);
			panel_2.add(table);
			table.setAutoResizeMode(table.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
			table.addMouseListener(this);
		} catch (SQLException e) {
			welcome.setVisible(true);
			this.dispose();
			e.printStackTrace();
		}
		contentPane.revalidate();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Back") {
			welcome.setVisible(true);
			this.dispose();
		} else if (((JButton) (arg0.getSource())).equals(btnAdd) || ((JButton) (arg0.getSource())).equals(btnEdit) || ((JButton) (arg0.getSource())).equals(btnDelete))  {
			if (((JButton) (arg0.getSource())).equals(btnAdd)) {
				edit = AddEditDel.ADD;
				btnAdd.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnEdit)) {
				edit = AddEditDel.EDIT;
				btnEdit.setVisible(false);
			} else if (((JButton) (arg0.getSource())).equals(btnDelete)) {
				edit = AddEditDel.DELETE;
				btnDelete.setVisible(false);
			}
			btnSubmit.setEnabled(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.ADD) {
			try {
				new user.Module(txtTitle.getText(), txtDepCode.getText(), Integer.parseInt(txtModCode.getText()), txtSemester.getText(), Integer.parseInt(txtCredits.getText()), chkTaught.isSelected());
			} catch (Exception e) {
				e.printStackTrace();
			} 
			btnAdd.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.EDIT) {
			try {
				user.Module module = new user.Module(txtDepCode.getText(), origModCode);
				module.update(txtTitle.getText(), txtDepCode.getText(), Integer.parseInt(txtModCode.getText()), txtSemester.getText(), Integer.parseInt(txtCredits.getText()), chkTaught.isSelected());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			btnEdit.setVisible(true);

		} else if (((JButton) (arg0.getSource())).getText() == "Submit" && edit == AddEditDel.DELETE) {
			try {
				user.Module module = new user.Module(txtDepCode.getText(), Integer.parseInt(txtModCode.getText()));
				module.delete();
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnDelete.setVisible(true);
		}
		displayTable();
		contentPane.revalidate();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent mouseEvent) {
        JTable table =(JTable) mouseEvent.getSource();
        Point point = mouseEvent.getPoint();
        int row = table.rowAtPoint(point);
        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
    			txtTitle.setText((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
        		txtDepCode.setText((String)table.getModel().getValueAt(table.getSelectedRow(), 1));
        		txtSemester.setText((String)table.getModel().getValueAt(table.getSelectedRow(), 2));
        		txtModCode.setText("" +(int)table.getModel().getValueAt(table.getSelectedRow(), 3));
        		txtCredits.setText("" +(int)table.getModel().getValueAt(table.getSelectedRow(), 4));
        		chkTaught.setSelected((Boolean.valueOf((String)table.getModel().getValueAt(table.getSelectedRow(), 5))));
        		origModCode = Integer.parseInt(txtModCode.getText());
        }
        contentPane.revalidate();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
