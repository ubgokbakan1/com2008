/** ManageUsersPage.java for managing users
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.xdevapi.Table;

import GUI.ManageDepartmentsPage.AddEditDel;
import user.Department;
import user.User;

public class ManageUsersPage extends JFrame implements ActionListener, MouseListener {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtType;
	private JFrame welcome;
	private JTable table;
	private JPanel panel_2, panel_9;
	private JButton btnCreate, btnDelete;
	private AddEditDel edit;

	
	private User user;
	/**
	 * Create the frame.
	 */
	public ManageUsersPage(JFrame welcomePage, User user) {
		welcome = welcomePage;
		this.user = user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(new BoxLayout(panel_8, BoxLayout.Y_AXIS));
		contentPane.add(panel_8, BorderLayout.CENTER);
		
		panel_9 = new JPanel();
		panel_8.add(panel_9);
		displayTable();
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(new FlowLayout());
		btnCreate = new JButton("Create a new user");
		btnCreate.addActionListener(this);
		btnDelete = new JButton("Delete a current user");
		btnDelete.addActionListener(this);
		panel_9.add(panel_5);
		panel_5.add(btnCreate);
		panel_5.add(btnDelete);
		
		JPanel panel_7 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_7.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_7, BorderLayout.NORTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(this);
		btnBack.setHorizontalAlignment(SwingConstants.LEFT);
		panel_7.add(btnBack);
	}
	
	/**
	 * For displaying the table that contains all user information extracted from the database
	 */
	private void displayTable() {
		String[] columnNames = {"Username", "User type"};
		String[][] data;
		panel_9.removeAll();
		try {
			ArrayList<User> dataList = User.getAll();
			data = new String[dataList.size()][2];
			for (int i=0; i< dataList.size(); i++) {
				data[i][0] = dataList.get(i).getUsername();
				data[i][1] = dataList.get(i).getType();
			}
			table = new JTable(data, columnNames);
			table.addMouseListener(this);
			panel_9.add(table);
		} catch (SQLException e) {
			welcome.setVisible(true);
			this.dispose();
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Back") {
			welcome.setVisible(true);
			this.dispose();
		} else if (((JButton) (arg0.getSource())).getText() == "Create a new user") {
			CreateUserPage create = new CreateUserPage(user);
			create.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Delete a current user") {
			edit = AddEditDel.DELETE;
		} if (((JButton) (arg0.getSource())).getText() == "Delete a current user" && edit == AddEditDel.DELETE) {
			try {
				User usr = new User((String) table.getModel().getValueAt(table.getSelectedRow(), 0), (String) table.getModel().getValueAt(table.getSelectedRow(), 1));
				usr.delete();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnDelete.setVisible(true);
			displayTable();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}