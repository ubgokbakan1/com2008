package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import user.Grade;
import user.Module;
import user.Period;
import user.Permissions;
import user.Student;
import user.User;

public class ViewStudentsPage extends JFrame implements ActionListener, MouseListener{

	private JPanel contentPane;
	private JPanel panel_1;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_6;
	private JFrame welcome;
	private JTextField txtGrade;
	private JTextField txtModuleCode;
	private JTextField txtPeriodKey;
	private JCheckBox chkCapped;
	private JButton btnEdit;
	private JButton btnAdd;
	private JButton btnPass;
	private JLabel lblStudentReg;
	private JTable table_2;
	private JTable table;
	
	private User user;
	
	/**
	 * Create the frame.
	 */
	public ViewStudentsPage(JFrame welcome, User user) {
		this.welcome = welcome;
		this.user = user;
		
		panel_6 = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		panel_1 = new JPanel();
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(this);
		btnBack.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(btnBack);
		
		if (user.getPermissionList().contains(Permissions.EDIT_STUDENTS)) {
			String[] columns = {"Title", "Registration No.", "Surname", "Forname", "Tutor", "Department Code", "Degree Code"};
			JPanel panel_2 = new JPanel();
			panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
			Object[][] data;
			try {
				ArrayList<Student> dataList = Student.getAllStudents();
				data = new Object[dataList.size()][7];
				for (int i=0; i< dataList.size(); i++) {
					data[i][0] = dataList.get(i).getTitle();
					data[i][1] = dataList.get(i).getRegistration();
					data[i][2] = dataList.get(i).getLastname();;
					data[i][3] = dataList.get(i).getForename();
					data[i][4] = dataList.get(i).getTutor();
					data[i][5] = dataList.get(i).getDepartment().getCode();
					data[i][6] = dataList.get(i).getDegree().getDegreeCode();
				}
				table = new JTable(data, columns);
				table.addMouseListener(this);
				panel_1.add(panel_2);
				panel_2.add(table);
				contentPane.revalidate();
			} catch (SQLException e) {
				welcome.setVisible(true);
				this.dispose();
				e.printStackTrace();
			}
			btnPass = new JButton("Progress Student");
			btnPass.addActionListener(this);
			panel_2.add(btnPass);
		} else if (user.getClass().equals(Student.class)) { //If logged in as student
			Student student = (Student) user;
			try {
				displayGrades(student, false);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}

	private void displayGrades(Student student, boolean editable) throws SQLException {
		JPanel panel_2 = new JPanel();
		try {
			panel_1.remove(1); // Clear layout
		} catch (Exception e) {
			
		}
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		// Get a list of all the students grades.
		ArrayList<Period> periodList = Period.getAll(student);
		ArrayList<Grade> gradeList = new ArrayList<Grade>();
		try {
			for (int i=0; i<periodList.size(); i++) {
				gradeList.addAll(periodList.get(i).getGrades());
			}
		} catch (Exception e) {
			
		}
		// Convert list into arrays to store in table
		String[] columnsGrade = {"Grade", "Module Code", "Period Key"};
		Object[][] data = new Object[gradeList.size()][4];
		for (int i=0; i<gradeList.size(); i++) {
			data[i][0] = Grade.getLast(gradeList.get(i).getPeriodModule().getPeriod(), gradeList.get(i).getModule()).getGrade();
			data[i][1] = ((Grade)gradeList.get(i)).getModule().getModuleCode();
			data[i][2] = ((Grade)gradeList.get(i)).getPeriodModule().getPeriod().getPeriodKey();
		}
		// Create table
		table_2 = new JTable(data, columnsGrade);
		panel_2.add(table_2);
		JPanel panel_5 = new JPanel();
		panel_5.add(new JLabel("Student Reg No: "));
		lblStudentReg = new JLabel(""+student.getRegistration());
		panel_5.add(lblStudentReg);
		
		panel_2.add(panel_5);
		panel_1.add(panel_2);
		if (editable) {
			table_2.addMouseListener(this);
			panel_3 = new JPanel();
			panel_3.add(new JLabel("Grade (0-100)"));
			txtGrade = new JTextField();
			txtGrade.setColumns(10);
			panel_3.add(txtGrade);
			
			panel_4 = new JPanel();
			panel_4.add(new JLabel("Module Code"));
			txtModuleCode = new JTextField();
			txtModuleCode.setColumns(10);
			panel_4.add(txtModuleCode);
			
			JPanel panel_6 = new JPanel();
			panel_6.add(new JLabel("Period Key"));
			txtPeriodKey = new JTextField();
			txtPeriodKey.setColumns(10);
			panel_6.add(txtPeriodKey);
			
			chkCapped = new JCheckBox("Capped");
			
			panel_2.add(panel_3);
			panel_2.add(panel_4);
			panel_2.add(panel_6);
			panel_2.add(chkCapped);
			
			btnEdit = new JButton("Add");
			btnEdit.addActionListener(this);
			panel_2.add(btnEdit);
		}
	}

	private void displayModules(Student student) throws SQLException {
		try {
			panel_1.remove(panel_6); // Clear layout
		} catch (Exception e) {
			
		}
		// Get a list of all the students modules.
		Period period = Period.getLast(student);

		// Convert list into arrays to store in table
		String[] columnsGrade = {"Module name", "Module Code"};
		Object[][] data = new Object[period.getModules().size()][2];
		for (int i=0; i<period.getModules().size(); i++) {
			data[i][0] = period.getModules().get(i).getTitle();
			data[i][1] = period.getModules().get(i).getModuleCode();
		}
		// Create table
		JTable table_r = new JTable(data, columnsGrade);
		panel_6.removeAll();
		panel_6.add(table_r);
		
		panel_1.add(panel_6);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Back") {
			welcome.setVisible(true);
			this.dispose();
		} else if (arg0.getSource().equals(btnEdit)) {
			try {
				// Get grade object to edit
				Student student = new Student(Integer.parseInt(lblStudentReg.getText()));
				Period period = new Period(txtPeriodKey.getText().charAt(0), student);
				Module module = new Module(student.getDepartment().getCode(), Integer.parseInt(txtModuleCode.getText())); 
				new Grade(period, module, Float.parseFloat(txtGrade.getText()));
				displayGrades(student, true);
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (((JButton) (arg0.getSource())).getText() == "Progress Student") {
			try {
				Student student = new Student(Integer.parseInt(lblStudentReg.getText()));
				boolean passedPeriod = student.getLastPeriod().canProceed(student.getAwardType());
				
				if (passedPeriod) {
					if (student.canGraduate()) {
						JOptionPane.showMessageDialog(null, "Student passed, now graduating", "Passed", JOptionPane.INFORMATION_MESSAGE);
						student.getAward();
					} else {
						JOptionPane.showMessageDialog(null, "Student passed, progressing onto next period", "Passed", JOptionPane.INFORMATION_MESSAGE);
						String startDate = JOptionPane.showInputDialog("Type start date: ");
						String endDate = JOptionPane.showInputDialog("Type end date: ");
						Period nextPeriod = student.getLastPeriod().nextLevel(new java.sql.Date(java.sql.Date.parse(startDate)), new java.sql.Date(java.sql.Date.parse(endDate)));
					}
				} else {
					if (student.getLastPeriod().isPassing()) {
						JOptionPane.showMessageDialog(null, "Not enough marked modules.", "Not enough credits", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Student now re-registered for this period due to failing", "Student resit", JOptionPane.INFORMATION_MESSAGE);
						
						String startDate = JOptionPane.showInputDialog("Type start date: ");
						String endDate = JOptionPane.showInputDialog("Type end date: ");
						student.getLastPeriod().repeatPeriod(new java.sql.Date(java.sql.Date.parse(startDate)), new java.sql.Date(java.sql.Date.parse(endDate)));
					}
				}
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		contentPane.revalidate();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	// handles table presses
	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		if (mouseEvent.getSource().equals(table_2)) {
			//Fill in the edit grade form
			if (mouseEvent.getClickCount() == 2 && table_2.getSelectedRow() != -1) {
				txtPeriodKey.setText(""+table_2.getModel().getValueAt(table_2.getSelectedRow(), 2));
				txtModuleCode.setText(""+table_2.getModel().getValueAt(table_2.getSelectedRow(), 1));
				txtGrade.setText(""+table_2.getModel().getValueAt(table_2.getSelectedRow(), 0));
			}
		} else if (mouseEvent.getSource().equals(table)){
	        Point point = mouseEvent.getPoint();
	        int row = table.rowAtPoint(point);
	        if (table.getSelectedRow() != -1) {
	    			try {
						Student student = new Student((int) table.getModel().getValueAt(table.getSelectedRow(), 1));
						displayGrades(student, true);
						displayModules(student);
					} catch (SQLException e) {
						e.printStackTrace();
					}
	        }		
		}
		contentPane.revalidate();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	

}