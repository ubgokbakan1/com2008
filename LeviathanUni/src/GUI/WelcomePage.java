/** WelcomePage.java 
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import user.Permissions;
import user.Student;
import user.User;

import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class WelcomePage extends JFrame implements ActionListener {

	private JPanel contentPane;
	private User user;
	
	/**
	 * Launch the application.
	 */
	public WelcomePage(User user) {
		this.user = user;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 851, 139);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblWelcome = new JLabel("Welcome " + user.toString());
		GridBagConstraints gbc_lblWelcome = new GridBagConstraints();
		gbc_lblWelcome.insets = new Insets(0, 0, 5, 5);
		gbc_lblWelcome.gridx = 4;
		gbc_lblWelcome.gridy = 7;
		panel.add(lblWelcome, gbc_lblWelcome);

		if (user.getPermissionList().contains(Permissions.EDIT_MODULES)) {
			JButton btnManageModules = new JButton("Manage Modules");
			btnManageModules.addActionListener(this);
			GridBagConstraints gbc_btnManageModules = new GridBagConstraints();
			gbc_btnManageModules.insets = new Insets(0, 0, 5, 5);
			gbc_btnManageModules.gridx = 1;
			gbc_btnManageModules.gridy = 8;
			panel.add(btnManageModules, gbc_btnManageModules);
		}
		
		if (user.getPermissionList().contains(Permissions.EDIT_DEPARTMENTS)) {
			JButton btnManageDepartments = new JButton("Manage Departments");
			btnManageDepartments.addActionListener(this);
			GridBagConstraints gbc_btnManageDepartments = new GridBagConstraints();
			gbc_btnManageDepartments.insets = new Insets(0, 0, 5, 5);
			gbc_btnManageDepartments.gridx = 3;
			gbc_btnManageDepartments.gridy = 8;
			panel.add(btnManageDepartments, gbc_btnManageDepartments);
		}
		
		if (user.getPermissionList().contains(Permissions.EDIT_USERS)) {
			JButton btnManageUsers = new JButton("Manage users");
			btnManageUsers.addActionListener(this);
			GridBagConstraints gbc_btnManageUsers = new GridBagConstraints();
			gbc_btnManageUsers.insets = new Insets(0, 0, 5, 5);
			gbc_btnManageUsers.gridx = 5;
			gbc_btnManageUsers.gridy = 8;
			panel.add(btnManageUsers, gbc_btnManageUsers);
		}
		
		if (user.getPermissionList().contains(Permissions.EDIT_DEGREES)) {
			JButton btnManageDegrees = new JButton("Manage Degrees");
			btnManageDegrees.addActionListener(this);
			GridBagConstraints gbc_btnManageDegrees = new GridBagConstraints();
			gbc_btnManageDegrees.insets = new Insets(0, 0, 5, 0);
			gbc_btnManageDegrees.gridx = 7;
			gbc_btnManageDegrees.gridy = 8;
			panel.add(btnManageDegrees, gbc_btnManageDegrees);
		}
		
		JButton btnViewStudentState = new JButton("View student state");
		btnViewStudentState.addActionListener(this);
		GridBagConstraints gbc_btnViewStudentState = new GridBagConstraints();
		gbc_btnViewStudentState.insets = new Insets(0, 0, 0, 5);
		gbc_btnViewStudentState.gridx = 4;
		gbc_btnViewStudentState.gridy = 9;
		panel.add(btnViewStudentState, gbc_btnViewStudentState);
		
		JButton btnStudentModules;
		if (user.getClass().equals(Student.class)) {
			btnStudentModules = new JButton("Register for period");
		} else {
			btnStudentModules = new JButton("Edit student modules");
		}
		btnStudentModules.addActionListener(this);
		GridBagConstraints gbc_btnStudentModules = new GridBagConstraints();
		gbc_btnStudentModules.insets = new Insets(0, 0, 0, 5);
		gbc_btnStudentModules.gridx = 5;
		gbc_btnStudentModules.gridy = 9;
		panel.add(btnStudentModules, gbc_btnStudentModules);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.WEST);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.NORTH);
		
		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3, BorderLayout.SOUTH);
		
		JPanel panel_4 = new JPanel();
		contentPane.add(panel_4, BorderLayout.EAST);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (((JButton) (arg0.getSource())).getText() == "Manage users") {
			ManageUsersPage manage = new ManageUsersPage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Manage Departments") {
			ManageDepartmentsPage manage = new ManageDepartmentsPage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Manage Modules") {
			ManageModulesPage manage = new ManageModulesPage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Manage Degrees") {
			ManageDegreesPage manage = new ManageDegreesPage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "View student state") {
			ViewStudentsPage manage = new ViewStudentsPage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} else if (((JButton) (arg0.getSource())).getText() == "Edit student modules" || ((JButton) (arg0.getSource())).getText() == "Register for period") {
			EditStudentModulePage manage = new EditStudentModulePage(this, user);
			this.setVisible(false);
			manage.setVisible(true);
		} 
	}
}
