/** Administrator.java for administrator management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;
import java.security.*;
import java.sql.SQLException;
import java.nio.charset.StandardCharsets;


public class Administrator extends User {
	
	public Administrator(String username) {
		super(username, "Administrator");
		permissionsList.add(Permissions.EDIT_DEGREES);
		permissionsList.add(Permissions.EDIT_DEPARTMENTS);
		permissionsList.add(Permissions.EDIT_MODULES);
		permissionsList.add(Permissions.EDIT_STUDENTS);
		permissionsList.add(Permissions.EDIT_USERS);
	}
	
	public Administrator(String username, String p) throws NoSuchAlgorithmException, SQLException {
		super(username, p, "Administrator");
		permissionsList.add(Permissions.EDIT_DEGREES);
		permissionsList.add(Permissions.EDIT_DEPARTMENTS);
		permissionsList.add(Permissions.EDIT_MODULES);
		permissionsList.add(Permissions.EDIT_STUDENTS);
		permissionsList.add(Permissions.EDIT_USERS);
	}
	
	/**Method for getting adding a user the the DB and assigning a user type
	 *
	 * @param username
	 * @param password
	 * @param type
	 * @param added
	 * @throws SQLException
	 */
	private boolean addUser(String username, String password, int type, boolean added) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "INSERT INTO users ('username', 'password', 'type') VALUES (?, ?, ?)";

		added = dac.execute(query, username, password, type);
		dac.closeConnection();

		return added;
	}
	
	/**Method for getting removing a user by username from the DB
	 *
	 * @param username
	 * @param success
	 * @throws SQLException
	 */
	private boolean removeUser(String username, boolean removed) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "DELETE FROM users WHERE ('username' = ?)";

		removed = dac.execute(query, username);
		dac.closeConnection();

		return removed;

	}
	/**Method for adding a module to the DB
	 *
	 * @param moduleCode
	 * @param credits
	 * @param semester
	 * @param added
	 * @throws SQLException
	 */
	public boolean addModule(String moduleCode, int credits, String semester, boolean added) {
		return added;
	}

	/**Method for removing a module from the DB
	 *
	 * @param moduleCode
	 * @param removed
	 * @throws SQLException
	 */
	public boolean removeModule(String moduleCode, boolean removed) {
		return removed;
	}

	/**Method for adding a department to the DB
	 *
	 * @param departmentCode
	 * @param added
	 * @throws SQLException
	 */
	public boolean addDepartment(String departmentCode, boolean added) {
		return added;
	}

	/**Method for removing a department from the DB
	 *
	 * @param departmentCode
	 * @param removed
	 * @throws SQLException
	 */
	public boolean removeDepartment(String departmentCode, boolean removed) {
		return removed;
	}

	/**Method for adding a course to the DB
	 *
	 * @param title
	 * @param moduleCode
	 * @param credits
	 * @param semester
	 * @param added
	 * @throws SQLException
	 */
	public boolean addCourse(String title, String moduleCode, int credits, String semester, boolean added) {
		return added;
	}

	/**Method for removing a course from the DB
	 *
	 * @param title
	 * @param removed
	 * @throws SQLException
	 */
	public boolean removeCourse(String title, boolean removed) {
		return removed;
	}

	/**Method for updating a user on the system
	 *
	 * @param usrnm
	 * @param type
	 * @throws SQLException
	 */
	public boolean update(String usrnm, String typ) throws SQLException {
		String query = "UPDATE users SET (username = ?, userType = ?) WHERE (username = ? AND userType = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		boolean success = dac.execute(query, usrnm, typ, username, userType);
		if(success) {
			username = usrnm;
			userType = typ;
		}
		dac.closeConnection();
		return success;
	}
}