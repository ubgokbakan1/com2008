/** Approval.java for degrees
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Approval {
	/**Approved enum*/
	public enum Approved {
		PENDING, REJECTED, APPROVED
	}
	//instance fields
	private int registration;
	private char periodKey;
	private Approved approval;

	/**
	 * Constructor Method, to get an existing Approval
	 * @param registrationNumber
	 * @param periodKey
	 * @throws SQLException
	 */
	public Approval(int reg, char pk) throws SQLException {
		registration = reg;
		periodKey = pk;

		//SQL operations
		String query = "SELECT approved FROM approval WHERE(registrationNumber = ? AND periodKey = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, registration, periodKey);

		//if there is an entry for that period, it is a binary Approved/Rejected
		if(rest.last()) {
			if(rest.getBoolean("approved"))
				approval = Approved.APPROVED;
			else
				approval = Approved.REJECTED;
		}
		//if there is no entry for that period, it is Pending
		else
			approval = Approved.PENDING;
		dac.closeConnection();
	}
	/**
	 * Constructor Method, to create a new Approval
	 * @param registrationNumber
	 * @param cperiodKey
	 * @param approved
	 * @throws SQLException
	 */
	public Approval(int reg, char k, boolean ap) throws SQLException{
		if(ap)
			approval = Approved.APPROVED;
		else
			approval = Approved.REJECTED;

		registration = reg;
		periodKey = k;

		//SQL operations
		String query = "INSERT INTO approval VALUES (registrationNumber = ?, periodKey = ?, approval = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		dac.execute(query, registration, periodKey, approval.toString());
		dac.closeConnection();
	}

	/**Returns true if approved*/
	public Approved isApproved() {
		return approval;
	}
	/**Approve or Reject method
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean approve() throws SQLException {
		approval = Approved.APPROVED;

		//SQL operations
		String query = "UPDATE approval SET (approval = ?) WHERE (registrationNumber = ? AND periodKey = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		boolean success = dac.execute(query,  approval.toString(), registration, periodKey);
		dac.closeConnection();

		return success;
	}
}