package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import user.Student.AwardType;

/**Award.java for Award class
 * 
 * @author Dean Knott, Travis Pell, Bora Gokbakan
 *
 */
public class Award {
	private static enum Awards{MERIT, DISTINCTION, FIRST, UPPER_SECOND, LOWER_SECOND, THIRD, PASS, FAIL}
	private AwardType awt;
	private Awards classGrade;
	private Degree degree;
	private Student student;
	private static float FIRST = 69.5f;
	private static float UPPER_SECOND = 59.5f;
	private static float LOWER_SECOND = 49.5f;
	private static float THIRD = 44.5f;
	private static float PASS = 39.5f;

	/**Constructor to award a degree to a student based on the mark guidelines
	 *
	 * @param student
	 * @throws SQLException
	 */
	public Award(Student s) throws SQLException {
		float average = s.getAverage();
		boolean graduate = s.canGraduate();
		degree = s.getDegree();
		awt = s.getAwardType();
		student = s;
		
		Period last = Period.getLast(student);
		
		if(graduate)
			classGrade = getAward(awt, average);
		else
			classGrade = Awards.FAIL;
	}
	
	public static Awards getAward(AwardType awt, float average) {
		switch(awt) {
		case U:
			if(average>=FIRST)
				return Awards.FIRST;
			else if(average>=UPPER_SECOND)
				return Awards.UPPER_SECOND;
			else if(average>=LOWER_SECOND)
				return Awards.LOWER_SECOND;
			else if(average>=THIRD)
				return Awards.THIRD;
			else if(average>=PASS)
				return Awards.PASS;
			else
				return Awards.FAIL;
		case P:
			if(average>=FIRST)
				return Awards.FIRST;
			else if(average>=UPPER_SECOND)
				return Awards.UPPER_SECOND;
			else if(average>=LOWER_SECOND)
				return Awards.LOWER_SECOND;
			else
				return Awards.FAIL;
		default:
			if(average>=FIRST)
				return Awards.DISTINCTION;
			else if(average>=UPPER_SECOND)
				return Awards.MERIT;
			else if(average>=LOWER_SECOND)
				return Awards.PASS;
			else
				return Awards.FAIL;
		}
	}
	public String title() {
		return student.awardPrefix() + " " + degree.getName();
	}
	public String award() {
		switch(classGrade) {
		case DISTINCTION:
			return " with Distinction";
		case MERIT:
			return " with Merit";
		case FIRST:
			return " with First Class Degree";
		case UPPER_SECOND:
			return " with Upper Second Class Degree";
		case LOWER_SECOND:
			return " with Lower Second Class Degree";
		case THIRD:
			return " with Third Class Degree";
		case PASS:
			return " with Pass (Non-Honours)";
		default:
			return " with Failed to Graduate";
		}
	}
	public String toString() {
		return title()+award();
	}

}
