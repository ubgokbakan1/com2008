/** DataAccess.java for data access
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class DataAccess {
	private static String HOST = "jdbc:mysql://stusql.dcs.shef.ac.uk/team047";
	private static String USERNAME = "team047";
	private static String PASSWORD = "ec3c84ec";

	private Connection con = null;
	private ResultSet rest = null;
	private PreparedStatement pst = null;


	//Constructor method
	DataAccess() {
	}

	/**Constructor for connecting to the database on the serve
	 *
	 * @throws SQLException
	 */
	public Connection openConnection() throws SQLException {
		con = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
		return con;
	}

	/**Constructor for closing the connection from the database server
	 *
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if (rest!=null)
			rest.close();
		if (pst!=null)
			pst.close();
		if (con!=null)
			con.close();
	}

	/**Method for getting the results of the sql queries
	 *
	 * @param query
	 * @param objects
	 * @throws SQLException
	 */
	public ResultSet getResult(String query,  Object...objects) throws SQLException{
		openConnection();
		pst = prepareStatement(query, objects);
		rest = pst.executeQuery();

		return rest;

	}

	/**Method for executing the sql queries
	 *
	 * @param query
	 * @param objects
	 * @throws SQLException
	 */
	public boolean execute(String query, Object...objects) throws SQLException {
		openConnection();
		pst = prepareStatement(query, objects);
		boolean result = pst.execute();
		closeConnection();

		return result;

	}

	/**Method for using prepared statements.
	 *
	 * @param query
	 * @param objects
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatement(String query, Object...objects) throws SQLException {
		openConnection();
		PreparedStatement ps = con.prepareStatement(query);
		int index = 1;
		for(Object o:objects) {
			if(o instanceof String) //if a String is passed on
				ps.setString(index, o.toString());
			else if(o instanceof Integer) //if an int is passed on
				ps.setInt(index, ((Integer) o).intValue());
			else if(o instanceof Character)
				ps.setString(index, ((Character) o).toString()); //if a char is passed on
			else if(o instanceof Date)
				ps.setDate(index,  (Date) o);
			else if(o instanceof Boolean)
				ps.setBoolean(index, (boolean) o);
			else if(o instanceof Float)
				ps.setFloat(index, (float) o);
			else if(o instanceof Double)
				ps.setDouble(index, (double) o);
			index++;
		}

		return ps;
	}
}
