package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/** Degree.java for degrees
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
public class Degree {
	private String departmentCode, underOrPost, degreeName;
	private int degreeCode;
	private boolean industryYear;
	private static String INDUSTRY_SUFFIX = " with a Year in Industry";
	private Department department;
	private ArrayList<Department> partnerDepartments = new ArrayList<Department>();
	/**Constructor for getting an existing Degree from DB
	 * 
	 * @param departmentCode
	 * @param degreeCode
	 * @throws SQLException
	 */
	public Degree(String depCd, int degCd) throws SQLException {
		this(new Department(depCd), degCd);
	}
	public Degree(Department dep, int degCd) throws SQLException {
		department = dep;
		departmentCode = department.getCode();
		degreeCode = degCd;
		
		DataAccess dac = new DataAccess();
		String query = "SELECT * FROM degree WHERE (departmentCode = ? AND degreeCode = ?);"; 

		ResultSet rest = dac.getResult(query, departmentCode, degreeCode);
		rest.next();
		
		underOrPost = rest.getString("underOrPost");
		degreeName = rest.getString("degreeName");
		degreeCode = rest.getInt("degreeCode");
		industryYear = rest.getBoolean("industryYear");
		rest.close();
		
		query = "SELECT * FROM partnerDepartment WHERE(degreeDepartment = ? AND degreeCode = ?);";
		rest = dac.getResult(query, department.getCode(), degreeCode);
		while(rest.next()) {
			String depCd = rest.getString("partnerDepartmentCode");
			partnerDepartments.add(new Department(depCd));
		}
		dac.closeConnection();
		
		if(industryYear)
			degreeName += INDUSTRY_SUFFIX;	
	}
	/**Constructor for creating a new Degree in DB
	 *@param degreeName
	 *@param departmentCode
	 *@param degreeCode
	 *@param industryYear
	 *@param underOrPost
	 *
	 */
	public Degree(String dgNm, Department dep, int dgCd, boolean indYear, char uOp, ArrayList<Department> partnerDept) throws SQLException {
		degreeName = dgNm;
		departmentCode = dep.getCode();
		degreeCode = dgCd;
		industryYear = indYear;
		underOrPost = uOp+"";
		
		DataAccess dac = new DataAccess();
		String query = "INSERT INTO degree (`departmentCode`, `underOrPost`, `degreeName`, `degreeCode`, `industryYear`) VALUES (?,?,?,?,?);";
		dac.execute(query, departmentCode, underOrPost, degreeName, degreeCode, industryYear);
		query = "INSERT INTO partnerDepartment (degreeDepartment, degreeCode, partnerDepartmentCode) VALUES (?,?,?);";
		
		if(partnerDept.size()>0) {
			for(Department d: partnerDept){
				dac.execute(query, d.getCode());
			}
		}
		
		dac.closeConnection();
		
		if(industryYear)
			degreeName += INDUSTRY_SUFFIX;
		
		
	}
	//get methods
	public String getName() {
		return degreeName;
	}
	
	public boolean getIndustry() {
		return industryYear;
	}
	
	public String getUnderPost() {
		return underOrPost;
	}
	
	public int getDegreeCode() {
		return degreeCode;
	}
	
	public Department getDeparment() throws SQLException {
		return new Department(departmentCode);
	}
	
	/**deletes the Degree, true if successful
	 * 
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean delete() throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "DELETE FROM degree WHERE (departmentCode = ? AND degreeCode = ?);";
		boolean result = dac.execute(query, departmentCode, degreeCode);
		dac.closeConnection();
		return result;
	}
	/**predicate to check if the same degreeCode exists
	 * 
	 * @param departmentCode
	 * @param degreeCode
	 * @return boolean
	 * @throws SQLException
	 */
	public static boolean exists(String depCd, int degCd) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "SELECT * FROM degree WHERE(departmentCode = ? AND degreeCode = ?);";
		ResultSet rest = dac.getResult(query, depCd, degCd);
		return rest.last();
	}
	/**returns an ArrayList<Degree> of all Degrees, static method
	 * 
	 * @return ArrayList<Degree>
	 * @throws SQLException
	 */
	public static ArrayList<Degree> getAll() throws SQLException {
		ArrayList<Degree> list = new ArrayList<Degree>();
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		ResultSet rest = dac.getResult("SELECT * FROM degree;");
		
		while(rest.next()) {
			String departmentCode = rest.getString("departmentCode");
			Department department = new Department(departmentCode);
			int degreeCode = rest.getInt("degreeCode");
			list.add(new Degree(department, degreeCode));
		}
		
		return list;
	}
	/** updates a Degree, true if successful
	 * 
	 * @param degreeName
	 * @param departmentCode
	 * @param degreeCode
	 * @param industryYear
	 * @param underOrPost
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean update(String dgNm, String dpCd, int dgCd, boolean indYear, char uOp) throws SQLException {
		String query = "UPDATE degree SET (degreeName = ?, departmentCode = ?, degreeCode = ?, industryYear=?, underOrPost = ?) WHERE (degreeName = ? AND departmentCode = ? AND degreeCode = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		boolean success = dac.execute(query,dgNm, dpCd, dgCd, indYear, uOp, degreeName, departmentCode, degreeCode);
		if(success) {
			degreeName = dgNm;
			departmentCode = dpCd;
			degreeCode = dgCd;
			industryYear = indYear;
			underOrPost = uOp+"";
		}
		dac.closeConnection();
		return success;
	}
	
}
