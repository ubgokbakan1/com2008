/** Department.java for department management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.*;
import java.util.ArrayList;

public class Department {
	
	private String departmentName, departmentCode;
	
	/**Constructor for getting the department code matching the appropriate name
	 *
	 * @param depCd
	 * @throws SQLException
	 */
	public Department(String depCd) throws SQLException {
		departmentCode = depCd;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		String query = "SELECT * FROM department WHERE (`departmentCode` = ?)";
		ResultSet rest = dac.getResult(query, departmentCode);
		
		rest.next();
		
		departmentName = rest.getString("departmentName");
		
		rest.close();
		dac.closeConnection();
	}
	
	/**Constructor for inserting new departments into the database
	 *
	 * @param depNm
	 * @param depCd
	 * @throws SQLException
	 */
	public Department (String depNm, String depCd) throws SQLException {
		departmentName = depNm;
		departmentCode = depCd;
		
		String query = "INSERT INTO department (departmentCode, departmentName) VALUES(?,?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		dac.execute(query, departmentCode, departmentName);
		dac.closeConnection();
	}
	
	/**Method the return all departments existing in the database as a list
	 *
	 * @throws SQLException
	 */
	public static ArrayList<Department> getAll() throws SQLException{
		ArrayList<Department> list = new ArrayList<Department>();
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		ResultSet rest = dac.getResult("SELECT departmentCode FROM department;");
		
		while(rest.next()) {
			String depCd = rest.getString("departmentCode");
			list.add(new Department(depCd));
		}
		dac.closeConnection();
		return list;
	}
	
	// Get methods
	public String getCode() {
		return departmentCode;
	}
	public String getName() {
		return departmentName;
	}
	
	/**Method for deleting departments from the database by using the departmentCode
	 *
	 * @throws SQLException
	 */
	public boolean delete() throws SQLException {
		String query = "DELETE FROM department WHERE (departmentCode = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		boolean success = dac.execute(query, departmentCode);
		if(success) {
			departmentCode = null;
			departmentName = null;
		}
		dac.closeConnection();
		return success;
		
	}
	
	/**Method for updating departments that exist in the database
	 *
	 * @throws SQLException
	 */
	public boolean update(String depCd, String depNm) throws SQLException {
		String query = "UPDATE department SET departmentCode = ?, departmentName = ? WHERE (departmentCode = ? AND departmentName = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		boolean success = dac.execute(query, depCd, depNm, departmentCode, departmentName);
		if(success) {
			departmentCode = depCd;
			departmentName = depNm;
		}
		dac.closeConnection();
		return success;
	}
}
