package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/** Grade.java for grade management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
public class Grade {
	private int id, periodModuleID;
	private float grade;
	private PeriodModule periodModule;
	private Period period;
	private boolean capped;
	
	//constructor to get an existing grade
	public Grade(int id) throws SQLException {
		String query = "SELECT * FROM grade WHERE (id = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, id);
		rest.next();
		this.id = id;
		
		grade = rest.getFloat("grade");
		capped = rest.getBoolean("capped");
		periodModuleID = rest.getInt("periodModule");
		periodModule = new PeriodModule(periodModuleID);
		rest.close();
		dac.closeConnection();
	}
	//constructor to create a new grade
	public Grade(PeriodModule pm, float gd) throws SQLException {
		periodModule = pm;
		
		grade = gd;
		boolean capped;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "SELECT COUNT(1) FROM grade WHERE (periodModule = ?);";
		ResultSet rest = dac.getResult(query, periodModule.getID());
		rest.last();
		int count = rest.getInt(1);
		rest.close();
		
		//if a grade exists for that module, the new one will be capped
		if(count==0)
			capped = false;
		else
			capped = true;
		
		query = "INSERT INTO grade (periodModule, grade, capped) VALUES (?,?,?);";
		dac.execute(query, periodModule.getID(), grade, capped);
		query = "SELECT id FROM grade WHERE (periodModule = ?) ORDER BY id DESC LIMIT 1;";
		rest = dac.getResult(query, periodModule.getID());
		rest.last();
		id = rest.getInt("id");
		rest.close();
		dac.closeConnection();
	}
	//constructor to create a new grade
	public Grade(Period p, Module m, float gd) throws SQLException {
		this(new PeriodModule(p, m), gd);
	}
	//get methods
	public int getID() {
		return id;
	}
	public float getGrade() {
		return grade;
	}
	public Module getModule() {
		return periodModule.getModule();
	}
	public PeriodModule getPeriodModule() throws SQLException {
		return periodModule;
	}
	public Period getPeriod() throws SQLException {
		return period;
	}
	public boolean isCapped() {
		return capped;
	}
	
	//to change the grade
	public boolean setGrade(float gd) throws SQLException {
		String query = "UPDATE grade SET (grade = ?) WHERE (id = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		boolean success = dac.execute(query, gd, id);
		if(success)
			grade = gd;
		dac.closeConnection();
		return success;
		
	}
	//static method that returns all the grades
	public static ArrayList<Grade> getAll(Period period, Module module) throws SQLException{
		PeriodModule pm = new PeriodModule(period, module);
		ArrayList<Grade> glist = new ArrayList<Grade>();
		
		String query = "SELECT * FROM grade WHERE (periodModule = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, pm.getID());
		
		while(rest.next()) {
			glist.add(new Grade(rest.getInt("id")));
		}
		rest.close();
		dac.closeConnection();
		
		return glist;
	}
	public static Grade getLast(Period period, Module module) throws SQLException{
		PeriodModule pm = new PeriodModule(period, module);
		
		String query = "SELECT * FROM grade WHERE (periodModule = ?) ORDER BY id DESC LIMIT 1;";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, pm.getID());
		rest.next();
		int id = rest.getInt(1);
		rest.close();
		dac.closeConnection();
		
		return new Grade(id);
	}
}
