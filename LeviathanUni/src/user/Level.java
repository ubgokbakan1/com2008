package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Level {
	
	private String deptCode;
	private int degreeCode;
	private char level;
	private List<ModulePick> coreModules, optionalModules;
	
	public Level(Degree deg, char lvl) throws SQLException {
		degreeCode = deg.getDegreeCode();
		deptCode = deg.getDeparment().getCode();
		level = lvl;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		String query = "SELECT * FROM level WHERE (deptCode = ?, degreeCode = ?, level = ?);";
		
		ResultSet rest = dac.getResult(query, deptCode, degreeCode, level);
		
		String deptCode;
		int moduleCode;
		boolean optional;

		
		while(rest.next()) {
			deptCode = rest.getString("moduleDept");
			moduleCode = rest.getInt("moduleCode");
			optional = rest.getBoolean("optional");
			
			if(optional)
				optionalModules.add(new ModulePick(deptCode, moduleCode, optional));
			else
				coreModules.add(new ModulePick(deptCode, moduleCode, optional));
		}
		dac.closeConnection();
	}
	public Level(Degree deg, char lvl, Module m, boolean opt) throws SQLException {
		degreeCode = deg.getDegreeCode();
		deptCode = deg.getDeparment().getCode();
		level = lvl;
		String modDept = m.getDepartmentCode();
		int modCd = m.getModuleCode();
		
		String query = "INSERT INTO level VALUES (?, ?, ?, ?, ?, ?)";
		DataAccess dac = new DataAccess();
		dac.execute(query, deptCode, degreeCode, level, modDept, modCd, opt);
		dac.closeConnection();
	}
	public boolean deleteModule(Module m) throws SQLException {
		String modDept = m.getDepartmentCode();
		int modCd = m.getModuleCode();
		
		String query = "DELETE FROM level WHERE (deptCode = ? AND degreeCode = ? AND level = ? AND moduleDept = ? AND moduleCode = ?)";
		DataAccess dac = new DataAccess();
		boolean success = dac.execute(query, deptCode, degreeCode, level, modDept, modCd);
		dac.closeConnection();
		
		return success;
	}
	
	public List<ModulePick> getCores() {
		return coreModules;
	}
	public List<ModulePick> getOptionals() {
		return optionalModules;
	} 		
}
