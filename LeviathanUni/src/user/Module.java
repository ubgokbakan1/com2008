package user;
import java.sql.*;
import java.util.ArrayList;
public class Module {
	private String title, departmentCode, semester;
	private int moduleCode, credits;
	private boolean taught;
	
	public Module(String depCd, int mdCd) throws SQLException {
		departmentCode = depCd;
		moduleCode = mdCd;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "SELECT * FROM module WHERE (departmentCode = ? AND moduleCode = ?);";

		ResultSet rest = dac.getResult(query, depCd, mdCd);
		if(rest.last()) {
			title = rest.getString("title");
			semester = rest.getString("semester");
			credits = rest.getInt("credits");
			taught = rest.getBoolean("taught");
		}
		rest.close();
		dac.closeConnection();
	}
	
	public Module(String t, String depCd, int mdCd, String smstr, int crdts, boolean tght) throws SQLException {
		departmentCode = depCd;
		moduleCode = mdCd;
		title = t;
		semester = smstr;
		credits = crdts;
		taught = tght;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "INSERT INTO module (`title`, `departmentCode`, `moduleCode`, `semester`, `credits`, `taught`) VALUES (?,?,?,?,?,?);";
		
		dac.execute(query, title, departmentCode, moduleCode, semester, credits, taught);
		dac.closeConnection();
	}
	
	public boolean update(String t, String depCd, int mdCd, String smstr, int crdts, boolean tght) throws SQLException {
		DataAccess dac = new DataAccess();
		
		String query = "UPDATE `team047`.`module` SET `title` = ?, `departmentCode`=?,`moduleCode`=?,`semester`=?,`credits`=?,`taught` = ? WHERE (`departmentCode` = ?) and (`moduleCode` = ?);" ;
		boolean success = dac.execute(query, t, depCd, mdCd, smstr, crdts, tght, departmentCode, moduleCode);
		if(success) {
			departmentCode = depCd;
			moduleCode = mdCd;
			title = t;
			semester = smstr;
			credits = crdts;
			taught = tght;
		}
		dac.closeConnection();
		return success;
	}

	public boolean delete() throws SQLException {
		String query = "DELETE FROM module WHERE (departmentCode = ? AND moduleCode = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		boolean success = dac.execute(query, departmentCode, moduleCode);
		dac.closeConnection();
		
		return success;
	}
	public String getTitle() {
		return title;
	}
	
	public String getDepartmentCode() {
		return departmentCode;
	}
	
	public String getSemester() {
		return semester;
	}
	
	public int getModuleCode() {
		return moduleCode;
	}
	
	public int getCredits() {
		return credits;
	}
	
	public boolean isTaught() {
		return taught;
	}
	public static ArrayList<Module> getAll() throws SQLException{
		ArrayList<Module> list = new ArrayList<Module>();
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		ResultSet rest = dac.getResult("SELECT * FROM module;");
		
		while(rest.next()) {
			String depCd = rest.getString("departmentCode");
			int moduleCode = rest.getInt("moduleCode");
			list.add(new Module(depCd, moduleCode));
		}
		dac.closeConnection();
		return list;
	}
	
}
