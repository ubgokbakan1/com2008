package user;

import java.sql.SQLException;

/**Module subclass with optional attribute*/
public class ModulePick extends Module{
	private boolean optional;
	
	public ModulePick(String deptCode, int moduleCode, boolean opt) throws SQLException {
		super(deptCode, moduleCode);
		optional = opt;
	}
	
	public boolean isOptional() {
		return optional;
	}
	

}
