/** ModuleRequest.java for module request management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModuleRequest {
	private int registration, totalCredits;
	private char periodKey;
	private ArrayList<Module> modulesRequested;

	/**Constructor for getting the modules belonging to a student with the matching registration number and period key
	 *
	 * @param reg
	 * @param k
	 * @throws SQLException
	 */
	public ModuleRequest(int reg, char k) throws SQLException {
		registration = reg;
		periodKey = k;

		modulesRequested = getModules();
	}

	/**Constructor for inserting module requests into the database
	 *
	 * @param reg
	 * @param k
	 * @param m
	 * @throws SQLException
	 */
	public ModuleRequest(int reg, char k, Module m) throws SQLException {
		registration = reg;
		periodKey = k;
		String departmentCode = m.getDepartmentCode();
		int moduleCode = m.getModuleCode();

		String query = "INSERT INTO moduleRequest VALUES (registrationNumber = ?, periodKey = ?, departmentCode = ?, moduleCode = ?);";

		DataAccess dac = new DataAccess();
		dac.openConnection();
		dac.execute(query, registration, periodKey, departmentCode, moduleCode);
		dac.closeConnection();

		modulesRequested = getModules();
	}

	/**Method for getting all modules requested from the module request table, adds them into a array list and returns the result
	 *
	 * @throws SQLException
	 */
	public ArrayList<Module> getModules() throws SQLException{
		ArrayList<Module> mlist = new ArrayList<Module>();
		String query = "SELECT * FROM moduleRequest WHERE (registrationNumber = ? AND periodKey = ?)";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, registration, periodKey);

		while(rest.next()) {
			String depCd = rest.getString("departmentCode");
			int modCd = rest.getInt("moduleCode");

			Module m = new Module(depCd, modCd);
			totalCredits += m.getCredits();
			mlist.add(m);
		}
		rest.close();
		dac.closeConnection();
		return mlist;
	}

	//get methods
	public int getRegistration() {
		return registration;
	}
	public char getPeriod() {
		return periodKey;
	}
	public Approval getApproval() throws SQLException {
		return new Approval(registration, periodKey);
	}
	
	public int getTotalCredits() {
		return totalCredits;
	}

}