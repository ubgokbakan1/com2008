/** Period.java for period management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.*;
import java.util.ArrayList;

import user.Student.AwardType;


public class Period {
	private static final int FIRST_YEAR_WEIGHT = 0;
	private static final int SECOND_YEAR_WEIGHT = 1;
	private static final int THIRD_YEAR_WEIGHT = 2;
	private static final int FOURTH_YEAR_WEIGHT = 2;
	private static final int POST_WEIGHT = 1;
	private char periodKey, level;
	private int registrationNumber, id;
	private Date startDate, endDate;
	private boolean passed, repeating;
	private ArrayList<Grade> moduleGrades = null;
	
	private static float POST_THRESHOLD = 49.5f;
	private static float PASS_THRESHOLD = 39.5f;
	private static float CONCEDED_THRESHOLD = PASS_THRESHOLD - 10.0f;
	private static int POST_FAIL = 15;
	private static int UNDER_FAIL = 20;

	/**Constructor for getting the period of study for a student
	 *
	 * @param pk
	 * @param student
	 * @throws SQLException
	 */
	public Period (int pid) throws SQLException {
		id = pid;
		
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "SELECT * FROM period WHERE (id = ?);";
		ResultSet rest = dac.getResult(query, id);
		rest.last();
		
		
		level = rest.getString("level").toCharArray()[0];
		startDate = rest.getDate("startDate");
		endDate = rest.getDate("endDate");
		passed = rest.getBoolean("passed");
		repeating = rest.getBoolean("repeating");
		registrationNumber = rest.getInt("registrationNumber");
		periodKey = rest.getString("periodKey").charAt(0);
		
		rest.close();
		dac.closeConnection();
		
		//studentModules = getModules();
		//moduleGrades = getGrades();
	
	}
	public Period(char pk, Student student) throws SQLException {
		registrationNumber = student.getRegistration();
		periodKey = pk;

		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "SELECT * FROM period WHERE (registrationNumber = ? AND periodKey = ?)";
		ResultSet rest = dac.getResult(query, registrationNumber, periodKey);

		rest.next();

		level = rest.getString("level").toCharArray()[0];
		startDate = rest.getDate("startDate");
		endDate = rest.getDate("endDate");
		passed = rest.getBoolean("passed");
		repeating = rest.getBoolean("repeating");
		id = rest.getInt("id");

		rest.close();
		dac.closeConnection();

	}

	/**Constructor for creating a new period
	 *
	 * @param reg
	 * @param sD
	 * @param eD
	 * @param lvl
	 * @throws SQLException
	 */
	public Period(int reg, Date sD, Date eD, char lvl, boolean repeating) throws SQLException {
		String query = "SELECT * FROM period WHERE (registrationNumber = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		registrationNumber = reg;
		startDate = sD;
		endDate = eD;
		level = lvl;

		ResultSet rest = dac.getResult(query, registrationNumber);
		if(rest.last()) {
			periodKey = rest.getString("periodKey").toCharArray()[0];
			periodKey++;
			passed = rest.getBoolean("passed");

			rest.close();
		}
		else {
			periodKey = 'A';
		}
		level = lvl;
		query = "INSERT INTO period (`periodKey`, `level`, `registrationNumber`, `startDate`, `endDate`, `passed`,`repeating`) VALUES (?,?,?,?,?,?);";
		dac.execute(query, periodKey, level, registrationNumber, startDate, endDate, false, repeating);
		
		query = "SELECT * FROM period WHERE (registrationNumber = ? AND periodKey = ?) ORDER BY periodKey DESC LIMIT 1;";
		ResultSet nrest = dac.getResult(query, registrationNumber, periodKey);
		nrest.last(); 
		id = nrest.getInt("id"); 
		rest.close();
		dac.closeConnection();	
	}
	
	public Period nextLevel(Date sD, Date eD) throws SQLException {
		setPassed();
		return new Period(registrationNumber, sD, eD, level++, false);
	}
	
	//get methods
	public char getPeriodKey() {
		return periodKey;
	}
	public char getLevelKey() {
		return level;
	}
	public void setPassed() throws SQLException {
		passed = true;
		String query = "UPDATE `team047`.`period` SET `passed` = ? WHERE (id = ?);"; 
		DataAccess dac = new DataAccess();
		dac.openConnection();
		dac.execute(query, true, id);
		dac.closeConnection();
	}

	/**Method for getting the modules belonging to a period, adds them into an array list and returns the result
	 *
	 * @param reg
	 * @param k
	 * @throws SQLException
	 */
	public ArrayList<Module> getModules() throws SQLException{
		ArrayList<Module> mlist = new ArrayList<Module>();
		
		String query = "SELECT * FROM periodModule WHERE (periodID = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		ResultSet rest = dac.getResult(query, id);

		while (rest.next()) {
			String deptCode = rest.getString("departmentCode");
			int moduleCode = rest.getInt("moduleCode");
			mlist.add(new Module(deptCode, moduleCode));
		}
		
		dac.closeConnection();
		return mlist;
	}
	
	public ArrayList<PeriodModule> getPeriodModules() throws SQLException{
		return PeriodModule.getAll(id);
	}
	
	public PeriodModule addModule(Module m) throws SQLException {
		return new PeriodModule(this, m);
	}

	/**Method for getting the grades
	 *
	 * @throws SQLException
	 */
	public ArrayList<Grade> getGrades() throws SQLException{
			ArrayList<Grade> glist = new ArrayList<Grade>();
			for(Module m : getModules()) {
				glist.add(Grade.getLast(this, m));
			}
		return glist;
	}
	//gets the average, hence the name
	public float getAverage() throws SQLException {
		int totalCredits = 0;
		float totalGrades = 0;
		
		for(Grade g:moduleGrades) {
			int credit = g.getModule().getCredits();
			float grade = g.getGrade();
			
			//if a grade is capped
			if(g.isCapped())
				grade = Math.min(grade, 40.0f);
			
			totalGrades += (grade*credit);
			totalCredits += credit;
		}
		
		return totalGrades/totalCredits;	
	}
	//how many credits a student has failed
	public int concededCredits() throws SQLException {
		int credits = 0;
		for(Grade g : getGrades()) {
			//checks if a module is failed, dissertations don't count, so the module needs to be taught
			if(g.getGrade()<PASS_THRESHOLD && g.getModule().isTaught())
				if(g.getGrade()<PASS_THRESHOLD-10)
					credits += 100; //if it's below PASS_THRESHOLD-10, we put in a big number so that the year's not passable
				credits += g.getModule().getCredits();
		}
		return credits;
	}
	public ArrayList<Grade> passedGrades() throws SQLException {
		ArrayList<Grade> passedGrades = new ArrayList<Grade>();
		for(Grade g : getGrades()) {
			if(g.getGrade() >= PASS_THRESHOLD) {
				passedGrades.add(g);
			}
		}
		return passedGrades;
	}

	public Period repeatPeriod(Date sD, Date eD) throws SQLException {
		ArrayList<Grade> passedGrades = passedGrades();
		//fourth year can't be repeated
		if(level !='4') {
			Period repeatedPeriod = new Period(registrationNumber, sD, eD, level, true);
			DataAccess dac = new DataAccess();
			int periodID = repeatedPeriod.getID();
		
			String query = "INSERT INTO PeriodModule VALUES(departmentCode = ?, moduleCode = ?, periodID = ?);";
			PreparedStatement pstModule = dac.prepareStatement(query);
		
			query = "INSERT INTO grade VALUES(periodModule = ?, grade = ?, capped = ?);";
			PreparedStatement pstGrade = dac.prepareStatement(query);
		
			for(Grade g : passedGrades) {
				Module m = g.getModule();
				float grade = g.getGrade();
				boolean cpd = g.isCapped();
				String periodKey = this.periodKey+"";
			
				//can't use DAC since we'll call the same PreparedStatement over and over.
				pstGrade.setString(1, m.getDepartmentCode());
				pstGrade.setInt(2, m.getModuleCode());
				pstGrade.setString(3, periodKey);
				pstGrade.setInt(4, registrationNumber);
				pstGrade.setFloat(5, grade);
				pstGrade.setBoolean(6, cpd);
				pstGrade.execute();
			
				pstModule.setString(1, m.getDepartmentCode());
				pstModule.setInt(2, m.getModuleCode());
				pstModule.setInt(3, periodID);
				pstModule.execute();
			}
			pstModule.close();
			dac.closeConnection();
			return repeatedPeriod;
		}
		else
			return null;
		
		
	}

	//predicate to check for any modules failed with a grade below conceded pass threshold
	public boolean belowConcededFail() throws SQLException {
		for(Grade g : getGrades()) {
			//checks if a module is failed too badly
			if(g.getGrade()<CONCEDED_THRESHOLD)
				return true;
		}
		return false;
	}
	public int getWeight() {
		switch (level) {
		case '1':
			return FIRST_YEAR_WEIGHT;
		case '2':
			return SECOND_YEAR_WEIGHT;
		case '3':
			return THIRD_YEAR_WEIGHT;
		case '4':
			return FOURTH_YEAR_WEIGHT;
		default:
			return POST_WEIGHT;
		}
	}
	
	public boolean canProceed(AwardType awTyp) throws SQLException {
		float average = getAverage();
		boolean terminal = isTerminal(awTyp);
		
		if(!isPassing())
			return false;
		else if(awTyp == AwardType.MSc) 
			return (average>=POST_THRESHOLD);
		
		else if(awTyp == AwardType.P && terminal) 
				return (average>=POST_THRESHOLD);
		
		else
				return (average>=PASS_THRESHOLD);	
			
	}
	public boolean isPassing() throws SQLException {
		float average = getAverage();
		Student student = new Student(registrationNumber);
		AwardType awt = student.getAwardType();
		
		if(awt==AwardType.U || !isTerminal(awt)) {
			return !(average<PASS_THRESHOLD);
		}
		else
			return !(average<POST_THRESHOLD);
	}
	public int getID() {
		return id;
	}

	//get methods
	public int getRegistration() {
		return registrationNumber;
	}
	public Date getStartDate() {
		return startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public boolean passed() {
		return passed;
	}
	public boolean repeating() {
		return repeating;
	}
	public boolean isTerminal(AwardType awTyp) {
		if(awTyp == AwardType.P && level == '4')
			return true;
		
		if(awTyp == AwardType.U && level == '3') 
			return true;
		
		if(awTyp == AwardType.MSc  && level == 'P')
			return true;
		else
			return false;
	}
	
	/**Method for updating the period of a module for a student
	 *
	 * @param pssd
	 * @param sd
	 * @param ed
	 * @throws SQLException
	 */
	public boolean update(boolean pssd, Date sd, Date ed) throws SQLException {
		String query = "UPDATE period SET (passed = ?, startDate = ?, endDate = ?) WHERE (registrationNumber = ? AND periodKey = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		boolean success = dac.execute(query, pssd, sd, ed, registrationNumber, periodKey);
		dac.closeConnection();
		if(success) {
			passed = pssd;
			startDate = sd;
			endDate = ed;
		}
		return success;
	}
	public int maxFailCredits() {
		if(level=='P' || level == '4')
			return POST_FAIL;
		else
			return UNDER_FAIL;
	}

	/**Method for getting all the students studying in a specified period
	 *
	 * @param reg
	 * @throws SQLException
	 */
	public static ArrayList<Period> getAll(Student student) throws SQLException{
		ArrayList<Period> periods = new ArrayList<Period>();
		String query = "SELECT * FROM period WHERE (registrationNumber = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		ResultSet rest = dac.getResult(query, student.getRegistration());

		while(rest.next()) {
			char periodKey = rest.getString("periodKey").charAt(0);
			periods.add(new Period(periodKey, student));
		}
		rest.close();
		dac.closeConnection();

		return periods;
	}
	public boolean getPassed() {
		return passed;
	}
	public static ArrayList<Period> getPassed(Student student) throws SQLException{
		ArrayList<Period> periods = getAll(student);
		ArrayList<Period> passedPeriods = new ArrayList<Period>();
		for(Period p:periods) {
			if(p.passed())
				passedPeriods.add(p);
		}
		return passedPeriods;
	}

	
	public static Period getLast(Student student) throws SQLException {
		String query = "SELECT id FROM period WHERE (registrationNumber = ?) ORDER BY id DESC LIMIT 1;";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		ResultSet rest = dac.getResult(query, student.getRegistration());
		rest.last();
		int pid = rest.getInt(1);
		rest.close();
		dac.closeConnection();

		return new Period(pid);
	}
}