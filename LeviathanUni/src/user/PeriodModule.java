package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PeriodModule {
	private Module module;
	private Period period;
	private int id;
	public PeriodModule(Period p, Module m) throws SQLException {
		period = p;
		module = m;
		int periodID = period.getID();
		String departmentCode = module.getDepartmentCode();
		int moduleCode = module.getModuleCode();
		DataAccess dac = new DataAccess();
		
		String query = "SELECT * FROM periodModule WHERE (periodID = ? AND departmentCode=? AND moduleCode=?) ORDER BY id DESC LIMIT 1;";
		ResultSet rest = dac.getResult(query, periodID, departmentCode, moduleCode);
		if(rest.last()) {
			id = rest.getInt("id");
			rest.close();
		}
		
		else {
			rest.close();
			query = "INSERT INTO periodModule (periodID, departmentCode, moduleCode) VALUES (?,?,?);";
			dac.execute(query, periodID, departmentCode, moduleCode);
			query = "SELECT id FROM periodModule WHERE (periodID = ? AND departmentCode = ? AND moduleCode = ?) ORDER BY id DESC LIMIT 1;";
			rest = dac.getResult(query, periodID, departmentCode, moduleCode);
			rest.last();
			id = rest.getInt(1);
			rest.close();
			dac.closeConnection();
		}
	}
	
	public PeriodModule(int pmid) throws SQLException{
		id = pmid;
		String query = "SELECT * FROM periodModule WHERE (id = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		//System.out.print("getting pm: " + id);
		ResultSet rest = dac.getResult(query, id);
		rest.next();
		int periodID = rest.getInt("periodID");
		String departmentCode = rest.getString("departmentCode");
		int moduleCode = rest.getInt("moduleCode");
		
		module = new Module(departmentCode, moduleCode);
		period = new Period(periodID);
		rest.close();
		dac.closeConnection();
	}
	public boolean drop() throws SQLException {
		String query = "DELETE FROM periodModule WHERE (id = ?);";
		DataAccess dac = new DataAccess();
		boolean success = dac.execute(query, id);
		dac.closeConnection();
		return success;
	}
	public int getID() {
		return id;
	}
	public Grade addGrade(float gd) throws SQLException {
		return new Grade(this, gd);
	}
	public ArrayList<Grade> getGrades() throws SQLException{
		return Grade.getAll(period, module);
	}

	public Period getPeriod() {
		return period;
	}
	public Module getModule() {
		return module;
	}
	public static ArrayList<PeriodModule> getAll(int periodID) throws SQLException {
		ArrayList<PeriodModule> pmlist = new ArrayList<PeriodModule>();
		String query = "SELECT id FROM periodModule WHERE (periodID = ?);";
		DataAccess dac = new DataAccess();
		ResultSet rest = dac.getResult(query, periodID);
		
		while(rest.next()) {
			int id = rest.getInt("id");
			pmlist.add(new PeriodModule(id));
		}
		dac.closeConnection();
		return pmlist;
	}
}
