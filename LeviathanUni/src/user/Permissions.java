package user;
import java.sql.SQLException;

//Enum method for setting the permissions
public enum Permissions {
	EDIT_STUDENTS,EDIT_MODULES,EDIT_DEPARTMENTS,EDIT_USERS,EDIT_DEGREES,EDIT_STUDENT_GRADES
}