/** Registrar.java for registrar management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.security.NoSuchAlgorithmException;
import java.sql.*;


public class Registrar extends User {
	
	private static int UNDER_CREDITS = 120;
	private static int POST_CREDITS = 180;
	
	/**Constructor for granting the edit users permission if the username is a registrar type
	 *
	 * @param username
	 */
	public Registrar(String username) {
		super(username, "Registrar");
		permissionsList.add(Permissions.EDIT_USERS);
	}
	
	/**Method for adding new modules
	 *
	 * @param newModule
	 * @throws SQLException
	 */
	public boolean addModule(Module newModule) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		String query = "INSERT INTO module ('id', 'title', 'deptcode', 'modulecode', 'credits', 'semester', 'taught') VALUES (?, ?, ?, ?, ?, ?, ?);";
				
		boolean success = dac.execute(query, newModule);
		
		dac.closeConnection();
		
		return success;
	}
	
	/**Method for deleting a module by title
	 *
	 * @param title
	 * @throws SQLException
	 */
	public boolean dropModule(String title) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		String query = "DELETE FROM module WHERE ('title' = ?)";
				
		boolean success = dac.execute(query, title);					
		dac.closeConnection();
		
		return success;

	}
	
	/**Method for registering a student
	 *
	 * @param newStudent
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException 
	 */
	public Student addStudent(String username, String password, String forename, String lastname, String title, String tutor, String departmentCode, String underOrPost, int degreeCode) throws SQLException, NoSuchAlgorithmException {
		return new Student(username, password, forename, lastname, title, tutor, departmentCode, underOrPost, degreeCode);
	}
	
	/**Method for removing a student
	 *
	 * @param registration
	 * @throws SQLException
	 */
	public boolean removeStudent(int registration) throws SQLException {
		Student student = new Student(registration);
		return student.delete();
	}
	
	/**Method to check to see if the credits match whether the student is an undergraduate or a postgraduate
	 *
	 * @param pk
	 * @param student
	 * @throws SQLException
	 */
	public boolean checkCredits(int registration) throws SQLException {
		Student student = new Student(registration);
		char periodKey = Period.getLast(student).getPeriodKey();
		
		ModuleRequest mr = new ModuleRequest(registration, periodKey);
		int credits = mr.getTotalCredits();
		
		if(student.underPostGrad() == "U") {
			if(credits==UNDER_CREDITS)
				return true;
			else
				return false;
		}
		else if(student.underPostGrad() == "P") {
			if(credits==POST_CREDITS)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	/**Method updating a students details
	 *
	 * @param usrnm
	 * @param typ
	 * @throws SQLException
	 */
	public boolean update(String un, String ut) throws SQLException {
		String query = "UPDATE users SET (username = ?, userType = ?) WHERE (username = ? AND userType = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		
		boolean success = dac.execute(query, un, ut, username, userType);
		if(success) {
			username = un;
			userType = ut;
		}
		dac.closeConnection();
		return success;
	}
	
}
