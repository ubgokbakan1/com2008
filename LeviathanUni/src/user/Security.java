/** Security.java for security management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.security.*;
import java.sql.*;
import java.util.Base64;
import java.nio.charset.StandardCharsets;

public class Security {
	private static String MAIL_SUFFIX = "@sheffield.ac.uk";

	//Not own code, sourced from https://www.baeldung.com/java-random-string
	public static String saltGenerate(int length) {

	    int leftLimit = 48; // char '0'
	    int rightLimit = 122; // char 'z'

		SecureRandom random = new SecureRandom();
	    StringBuilder buffer = new StringBuilder(length);

	    for (int i = 0; i < length; i++) {
	        int randomLimitedInt = leftLimit + (int)
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();

	    return(generatedString);
	}

	/**Method to one way hash encrypt a string message
	 *
	 * @param message
	 * @throws NoSuchAlgorithmException
	 */
	public static String digest(String message) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
		String encoded = Base64.getEncoder().encodeToString(hash);

		return encoded;
	}

	/**Method to securely allow a user to log into the system
	 *
	 * @param username
	 * @param password
	 * @param SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public static User login(String username, String password) throws SQLException, NoSuchAlgorithmException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String userType;
		String query = "SELECT (passwordSalt) FROM user WHERE (username = ?)";
		ResultSet rest = dac.getResult(query, username);

		if(!rest.last())
			return null;
		else {
			String salt = rest.getString("passwordSalt");
			rest.close();

			query = "SELECT userType FROM user WHERE (username = ? AND password =? )";
			password = digest(password+salt);

			rest = dac.getResult(query, username, password);

			if(!rest.last())
				return null;
			else {
				userType = rest.getString("userType");
				rest.close();
				dac.closeConnection();
			}

			switch(userType) {
			case "Administrator":
				return new Administrator(username);

			case "Registrar":
				return new Registrar(username);

			case "Teacher":
				return new Teacher(username);

			case "Student":
				return new Student(username);
			}
		}
		return null;
	}

	/**Method to get the email based on the users forename and lastname
	 *
	 * @param forename
	 * @param lastname
	 * @throws SQLException
	 */
	public static String email(String forename, String lastname) throws SQLException {
		String[] names = forename.split(" ");
		forename = "";
		for(String s : names) {
			s = s.substring(0, 1);
			forename += s;
		}
		String email = forename+lastname;
		String query = "SELECT COUNT(*) FROM student WHERE (email LIKE ?);";
		DataAccess dac = new DataAccess();
		ResultSet rest = dac.getResult(query, email+'%');
		rest.next();
		int count = rest.getInt(1);
		rest.close();
		dac.closeConnection();
		if(count==0)
			email = email+MAIL_SUFFIX;
		else
			email = email+count+MAIL_SUFFIX;
		return email;
	}

	/**Method to the registration number for the student matching the username
	 *
	 * @param username
	 * @throws SQLException
	 */
	public static int getRegistration(String username) throws SQLException {
		DataAccess dac = new DataAccess();
		ResultSet rest = dac.getResult("SELECT registrationNumber FROM student WHERE (username = ?)", username);

		rest.next();
		int registration = rest.getInt("registrationNumber");
		rest.close();
		dac.closeConnection();

		return registration;
	}

	/**Method to get the username of a student matching the specified registration number
	 *
	 * @param message
	 * @throws NoSuchAlgorithmException
	 */
	public static String getUsername(int registration) throws SQLException{
		DataAccess dac = new DataAccess();
		ResultSet rest = dac.getResult("SELECT username FROM student WHERE (registrationNumber = ?)", registration);

		rest.next();
		String username = rest.getString("username");
		rest.close();
		dac.closeConnection();

		return username;
	}
}