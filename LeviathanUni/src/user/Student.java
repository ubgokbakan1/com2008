/** Student.java for student management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Student extends User {
	public enum AwardType {U, P, MSc, PGDip};
	private String forename, lastname, email, title, tutor, departmentCode,  underPostGrad;
	private int degreeCode, registrationNumber;
	private String userType = "Student";
	private AwardType awardType;

	/**Constructor for getting all student information in the database
	 *
	 * @param un
	 * @throws SQLException
	 */
	public Student(String un) throws SQLException {
		super(un, "Student");

		DataAccess dac = new DataAccess();
		ResultSet rest = dac.getResult("SELECT * FROM student WHERE (username = ?)", un);

		if(rest.last()){
			registrationNumber = rest.getInt("registrationNumber");
			forename = rest.getString("forename");
			lastname = rest.getString("lastname");
			email = rest.getString("email");
			title = rest.getString("title");
			departmentCode = rest.getString("departmentCode");
			degreeCode = rest.getInt("degreeCode");
			underPostGrad = rest.getString("underPostGrad");
			tutor = rest.getString("tutor");
			awardType = getAwardType(underPostGrad);
		}
		rest.close();
		dac.closeConnection();
	}

	/**Constructor for getting the course and level of study of the students
	 *
	 * @param username
	 * @param password
	 * @param forename
	 * @param lastname
	 * @param title
	 * @param tutor
	 * @param depCode
	 * @param uOp
	 * @param degreeCd
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException 
	 */
	public Student(String un, String pw, String fn, String ln, String tt, String tutr, String depCd, String uOp, int degCd) throws SQLException, NoSuchAlgorithmException {
		super(un, pw, "Student");

		forename = fn;
		lastname = ln;
		title = tt;
		departmentCode = depCd;
		degreeCode = degCd;
		underPostGrad = uOp;
		tutor = tutr;
		email = Security.email(forename, lastname);

		String query = "INSERT INTO student (`username`, `title`, `forename`, `lastname`, `departmentCode`, `degreeCode`, `tutor`, `underPostGrad`, `email`) "
				+ "VALUES (?,?,?,?,?,?,?,?,?);";
		DataAccess dac = new DataAccess();
		dac.execute(query, username, title, forename, lastname, departmentCode, degreeCode, tutor, underPostGrad, email);
		dac.closeConnection();
	}

	/**Method for getting all student from the database, add them to an array list and return the result
	 *
	 * @throws SQLException
	 */
	public static ArrayList<Student> getAllStudents() throws SQLException{
		ArrayList<Student> list = new ArrayList<Student>();
		DataAccess dac = new DataAccess();
		dac.openConnection();

		ResultSet rest = dac.getResult("SELECT * FROM user WHERE userType='Student'");

		while(rest.next()) {
			String username = rest.getString("username");
			list.add(new Student(username));
		}
		
		rest.close();
		dac.closeConnection();
		return list;
	}
	public AwardType getAwardType() {
		return awardType;
	}
	public static AwardType getAwardType(String awTyp) {
		switch(awTyp) {
		case "U":
			return AwardType.U;
		case "P":
			return AwardType.P;
		default:
			return AwardType.MSc;
		}
	}

	/**Method for updating all student credentials
	 *
	 * @param un
	 * @param fn
	 * @param ln
	 * @param tt
	 * @param tutr
	 * @param depCd
	 * @param uOp
	 * @param degCd
	 * @throws SQLException
	 */
	public boolean update(String un, String fn, String ln, String tt, String tutr, String depCd, String uOp, int degCd) throws SQLException {
		String query = "UPDATE student SET (`username`, `title`, `forename`, `lastname`, `departmentCode`, `degreeCode`, `tutor`, `underPostGrad`) "
				+ "VALUES (?,?,?,?,?,?,?,?);";

		DataAccess dac = new DataAccess();
		boolean success = dac.execute(query, un, tt, fn, ln, depCd, degCd, tutr, uOp);
		dac.closeConnection();

		if(success) {
			forename = fn;
			lastname = ln;
			title = tt;
			departmentCode = depCd;
			degreeCode = degCd;
			underPostGrad = uOp;
			tutor = tutr;
		}

		return success;
	}

	//get methods
	public Student(int registration) throws SQLException {
		this(Security.getUsername(registration));
	}

	public Degree getDegree() throws SQLException {
		return new Degree(departmentCode, degreeCode);
	}

	public Department getDepartment() throws SQLException {
		return new Department(departmentCode);
	}

	public String getForename() {
		return forename;
	}

	public String getLastname() {
		return lastname;
	}

	public String getTutor() {
		return tutor;
	}

	public String underPostGrad() {
		return underPostGrad;
	}

	public String getEmail() {
		return email;
	}

	public String getTitle() {
		return title;
	}
	public String getUsername() {
		return username;
	}
	public int getRegistration() {
		return registrationNumber;
	}
	public String getType() {
		return userType;
	}
	public ArrayList<Period> getPeriods() throws SQLException{
		return Period.getAll(this);
	}
	public float getAverage() throws SQLException {
		ArrayList<Period> plist = getPeriods();
		float grade = 0;
		int weight = 0;
		for(Period p : plist) {
			if(p.passed()) {
				grade = grade + (p.getAverage() * p.getWeight());
				weight += weight;
			}
		}
		return grade/weight;
	}
	
	public float getGPA() throws SQLException {
		int totalWeight= 0;
		float totalAverage = 0;
		ArrayList<Period> plist = Period.getPassed(this);
		
		for(Period p: plist) {
			int weight = p.getWeight();
			totalWeight = totalWeight + weight;
			totalAverage = p.getAverage()*weight;
		}
		
		return totalAverage/totalWeight;
	}

	public boolean canGraduate() throws SQLException {
		Period last = Period.getLast(this);
		if(last.isTerminal(awardType) && last.canProceed(awardType))
			return true;
		else
			return false;
	}
	public String awardPrefix() {
		switch(awardType) {
		case U:
			return "BSc";
		case PGDip:
			return "PGDip";
		default:
			return "MSc";
		}
	}
	public ArrayList<Period> getAllPeriods() throws SQLException{
		return Period.getAll(this);
	}
	public Period getLastPeriod() throws SQLException {
		return Period.getLast(this);
	}

	public Award getAward() throws SQLException {
		return new Award(this);
	}
}