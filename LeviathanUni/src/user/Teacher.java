/** Teacher.java for teacher management
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.sql.*;

public class Teacher extends User {

	public static boolean success;
	public static String username;
	public static float grade;

	/**Constructor for getting a teacher and assigning the edit student grades permission
	 *
	 * @param username
	 */
	public Teacher(String username) {
		super(username, "Teacher");
		permissionsList.add(Permissions.EDIT_STUDENT_GRADES);
	}

	/**Method for getting and returning a check to see if a student has passed
	 *
	 * @param passed
	 * @throws SQLException
	 */
	public boolean checkPass(boolean passed) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "SELECT * FROM period WHERE (`passed` = ?)";
		ResultSet res = dac.getResult(query, passed);

		res.next();

		success = res.getBoolean("passed");

		res.close();
		dac.closeConnection();

		return success;
	}

	/**Method for adding a grade a student has achieved in a module
	 *
	 * @param newGrade
	 * @throws SQLException
	 */
	public boolean addGrade(Grade newGrade) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "INSERT INTO grade ('id', 'period', 'deptcode', 'modulecode', 'grade') VALUES (?, ?, ?, ?, ?)";

		success = dac.execute(query, newGrade);

		dac.closeConnection();

		return success;

	}

	/**Method for updating a grade of a student
	 *
	 * @param newGrade
	 * @throws SQLException
	 */
	public boolean updateGrade(boolean newGrade) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "UPDATE grade SET grade	= ? WHERE (departmentCode = ? AND moduleCode = ?)";

		success = dac.execute(query);

		dac.closeConnection();

		return success;
	}

	/**Method for updating the weighted mean grade of a student
	 *
	 * @param mean
	 * @throws SQLException
	 */
	public boolean weightedMean(String mean) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "UPDATE grade SET degree	= ? WHERE (grade = ? AND moduleCode = ?)";

		success = dac.execute(query, mean);
		dac.closeConnection();

		return success;

	}


	/**Method for registering a student to a period of study if they have passed the previous year
	 *
	 * @param un
	 * @throws SQLException
	 */
	public boolean registerStudent(Student regStudent) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();

		String query = "INSERT INTO student ('registrationNumber', 'title', 'forename', 'surname', 'tutor', 'degreeCode', 'deptCode', 'underOrPostgrad', 'email') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		success = dac.execute(query);
		dac.closeConnection();

		return success;
	}

	/**Method to award a degree to a student based on the mark guidelines
	 *
	 * @param mean
	 * @param classGrade
	 * @throws SQLException
	 */
	public boolean awardDegree(int mean, String classGrade) throws SQLException {
		DataAccess dac = new DataAccess();
		dac.openConnection();
		String query = "SELECT AVG(grade) FROM grade";

		ResultSet res = dac.getResult(query);

		while(res.next()) {
			success = dac.execute(query, mean);
			mean = res.getInt("AVG(grade)");
		}

		if(mean < 39.5) {
			classGrade = "Fail";
		} else if (mean > 39.5 && mean <= 44.4) {
			classGrade = "Pass";
		} else if (mean >= 44.5 && mean <= 49.4) {
			classGrade = "Third class";
		} else if (mean >= 49.5 && mean <= 59.4) {
			classGrade = "Lower second";
		} else if (mean >= 59.5 && mean <= 69.4) {
			classGrade = "Upper second";
		} else if (mean >= 69.5) {
			classGrade = "First class";
		}


		res.close();
		dac.closeConnection();

		return success;
	}

	/**Method updating a students grade
	 *
	 * @param un
	 * @throws SQLException
	 */
	public boolean update(float grd) throws SQLException {
		String query = "UPDATE students SET (grade = ?) WHERE (grade = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		boolean success = dac.execute(query, grd, grade);
		if(success) {
			grade = grd;
		}
		dac.closeConnection();
		return success;
	}

}

