/** User.java to manage all users
 * @author Bora Gokbakan, Dean Knott, Travis Pell
 * */
package user;

import java.security.NoSuchAlgorithmException;

import java.sql.*;
import java.util.ArrayList;

public class User {
	protected String username, userType;
	protected ArrayList<Permissions> permissionsList;

	/**Constructor for getting a user and assigning the appropriate permission
	 *
	 * @param un
	 * @param ut
	 */
	public User(String un, String ut) {
		username = un;
		userType = ut;
		permissionsList = new ArrayList<Permissions>();
	}

	/**Constructor for inserting a new user into the database with a username, password and a user type
	 *
	 * @param un
	 * @param pw
	 * @param ut
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	public User(String un, String pw, String ut) throws SQLException, NoSuchAlgorithmException {
		String password, salt;
		username = un;
		password = pw;
		userType = ut;

		salt = Security.saltGenerate(12);

		String query = "INSERT INTO `team047`.`user` (`username`, `password`, `userType`, `passwordSalt`) VALUES (?,?,?,?);";

		password = Security.digest(password+salt);
		DataAccess dac = new DataAccess();
		dac.openConnection();

		dac.execute(query, username, password, userType, salt);
		dac.closeConnection();
	}

	//get methods
	public ArrayList<Permissions> getPermissionList(){
		return permissionsList;
	}

	public String getUsername() {
		return username;
	}
	public String getType() {
		return userType;
	}

	/**Method for getting all users, store them in an array list and return the result
	 *
	 * @throws SQLException
	 */
	public static ArrayList<User> getAll() throws SQLException {
		ArrayList<User> list = new ArrayList<User>();
		DataAccess dac = new DataAccess();
		dac.openConnection();

		ResultSet rest = dac.getResult("SELECT * FROM user;");

		while(rest.next()) {
			String username = rest.getString("username");
			String userType = rest.getString("userType");
			list.add(new User(username, userType));
		}
		dac.closeConnection();
		return list;
	}

	/**Method for updating the user credentials username and user type
	 *
	 * @param un
	 * @param ut
	 * @throws SQLException
	 */
	public boolean update(String un, String ut) throws SQLException {
		String query = "UPDATE user SET (username = ?, userType = ?) WHERE (username = ? AND userType = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();

		boolean success = dac.execute(query,un, ut, username, userType);
		if(success) {
			username = un;
			userType = ut;
		}
		dac.closeConnection();
		return success;
	}

	/**Method for deleting a user by username
	 *
	 * @throws SQLException
	 */
	public boolean delete() throws SQLException {
		String query = "DELETE FROM user WHERE (username = ?);";
		DataAccess dac = new DataAccess();
		dac.openConnection();
		boolean success = dac.execute(query,username);

		if(success){
			if(userType == "Student") {
				Student student = new Student(username);
				student.delete();
			}
		username = null;
		userType = null;
		}
		dac.closeConnection();
		return success;
	}

}
